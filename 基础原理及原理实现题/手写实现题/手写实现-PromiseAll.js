// Promise.myall = function(args) {
//     return new Promise((resolve, reject) => {
//         let index = 0;
//         let count = 0;
//         const =
//     })
// }

let p1 = new Promise((resolve, reject) => {
    resolve('ok1');
})

let p2 = new Promise((resolve, reject) => {
    // reject('error');
    resolve('ok2');
})

// // Promise.all([p2, p1, 1, 2, 3]).then(data => {
// //     console.log('resolve', data);
// // }, err => {
// //     console.log('reject', err);
// // })

// Promise.myPromiseAll = function(args) {
//     return new Promise((resolve, reject) => {
//         let count = 0;
//         const arr = [];
//         let errorCount = 0;
//         for (let i = 0; i < args.length; i++) {
//             Promise.resolve(args[i]).then(res => {
//                 // arr.push(res);
//                 arr[i] = res;
//                 count++;
//                 if (count === args.length) {
//                     resolve(arr);
//                 }
//             }).catch(err => {
//                 errorCount++;
//                 if (errorCount > 2) {
//                     reject(err);
//                 }
//             })
//         }
//         if (args.length === 0) {

//             resolve([]);
//         }
//     })
// }

Promise.myPromiseAll([p2, p1, 1, 2, 3]).then(data => {
    console.log('resolve', data);
}, err => {
    console.log('reject', err);
})

Promise.all([p2, p1, 1, 2, 3]).then(data => {
    console.log('resolve', data);
}, err => {
    console.log('reject', err);
})

Promise.MyAll = function(args) {
    return new Promise((resolve, reject) => {
        let count = 0;
        const arr = [];
        for (let i = 0; i < args.length; i++) {
            Promise.resolve(args[i]).then(res => {
                arr[i] = res;
                count++;
                if (count == args.length) {
                    resolve(arr);
                }
            }).catch(err => {
                reject(err);
            })
        }
        if (args.length == 0) {
            resolve([]);
        }
    })
}