function asyncPool(poolLimit, array, iteratorFn) {
    let i = 0;
    const ret = [];
    const executing = [];
    const enqueue = function() {
        if (i === array.length) {
            return Promise.resolve();
        }
        const item = array[i++];
        const p = Promise.resolve().then(() => iteratorFn(item));
        ret.push(p);
        const e = Promise.resolve().then(() => executing.splice(executing.indexOf(e), 1));
        executing.push(e);
        let r = Promise.resolve();
        if (executing.length >= poolLimit) {
            Promise.race(executing);
        }
        return r.then(() => enqueue());
    }
    return enqueue().then(() => Promise.all(ret));
}

const timeout = i => new Promise(resolve => {
    console.log(i);
    setTimeout(() => resolve(i), i);
});
const data = Date.now();

asyncPool(2, [1000, 5000, 3000, 2000], timeout).then(results => {
    console.log(results);
    console.log(Date.now() - data);
});