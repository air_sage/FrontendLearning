// Promise.resolve(1)
//     .then(res => {
//         console.log(res)
//         return 2
//     })
//     .then(res => {
//         console.log(res)
//     })

var str1 = 'abc';
var str2 = String('abc')
var str3 = new String('abc')

console.log(str1 === str2)
console.log(str2 === str3)
console.log(str1 === str3)
console.log(typeof str3)