class EventEmitter {
    constructor() {
        this.events = {};
    }
    on(eventName, callback) {
        if (!this.events[eventName]) {
            this.events[eventName] = [callback];
        } else {
            this.events[eventName].push(callback);
        }
    }
    emit(eventName) {
        this.events[eventName] && this.events[eventName].forEach(cb => cb());
    }
    removeListener(eventName, callback) {
        if (this.events[eventName]) {
            this.events[eventName] = this.events[eventName].filter(cb => cb != callback);
        }
    }
    once(eventName, callback) {
        let fn = () => {
            callback();
            this.removeListener(eventName, fn);
        }
        this.on(eventName, fn);
    }
}


let em = new EventEmitter();
let workday = 0;
em.on('work', function() {
    workday++;
    console.log('workeveryday');
})

em.once('live', function() {
    console.log('live only has once');
})

function makeMoney() {
    console.log('make one million money');
}

em.on('money', makeMoney);

let timer = setInterval(() => {
    em.emit('work');
    em.removeListener('money', makeMoney);
    em.emit('money');
    em.emit('live');
    if (workday === 5) {
        console.log('have a rest');
        clearInterval(timer);
    }
}, 1000);