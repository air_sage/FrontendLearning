// 简单选择排序 每一趟选出最小值
function selectSort(arr) {
    for (let i = 0; i < arr.length; i++) {
        let p = i;
        let min = arr[i];
        for (let j = i; j < arr.length; j++) {
            if (min > arr[j]) {
                p = j;
                min = arr[j];
            }
        }
        if (p !== i) {
            [arr[i], arr[p]] = [arr[p], arr[i]];
        }
    }
    return arr;
}

let res = selectSort([4, 6, 8, 4, 5, 74, 8, 2, 7, 65])
console.log(res);

// 选择排序优化：一趟同时选出最大最小值
function selectSort1(arr) {
    let i = 0;
    let j = arr.length - 1;
    while (i < j) {
        let min = arr[i];
        let max = arr[j];
        let p1 = i;
        let p2 = j;
        for (let k = i; k <= j; k++) {
            if (min > arr[k]) {
                min = arr[k];
                p1 = k;
            }
            if (max < arr[k]) {
                max = arr[k];
                p2 = k;
            }
        }
        // 如果首位和末位刚好是最大值和最小值，两者直接互换
        if (p1 == j && p2 == i) {
            [arr[i], arr[j]] = [arr[j], arr[i]];
        } else {
            if (p1 !== i)[arr[i], arr[p1]] = [arr[p1], arr[i]];
            if (p2 !== j)[arr[j], arr[p2]] = [arr[p2], arr[j]];
        }
        i++;
        j--;
        // console.log(arr);
    }
    return arr;
}

let res1 = selectSort1([4, 6, 8, 4, 5, 74, 8, 2, 7, 65]);
console.log(res1);