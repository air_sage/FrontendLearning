Function.prototype.myCall = function(context, ...args) {
    context = context || window;
    let key = Symbol(1);
    context[key] = this;
    let fn = context[key](...args);
    delete context[key];
    return fn;
}

Function.prototype.myCall1 = function(context, ...args) {
    context = context || window;
    // let obj = {};
    // obj.__proto__ = context;
    let obj = Object.create(context);
    obj.fn = this;
    let res = obj.fn(...args);
    obj = null;
    return res;
}

function sayHelloTo(to) {
    console.log(`${this.name} say hello to ${to}`)
}

var Jerry = {
    name: 'Jerry'
};
sayHelloTo.myCall1(Jerry, 'Tom');