var _const = function(data, value) {
    window.data = value;
    Object.defineProperty(window, data, {
        enumerable: false,
        configurable: false,
        get: function() {
            return value;
        },
        set: function(data) {
            if (data !== value) {
                throw new TypeError('Assignment to constant variable.');
            } else {
                return value;
            }
        }
    })
}