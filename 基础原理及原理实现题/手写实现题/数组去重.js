// 利用Set去重 缺点： 无法去掉“{}”空对象
function unique1(arr) {
    return Array.from(new Set(arr));
}
var arrBefore = [1, 1, 'true', 'true', true, true, 15, 15, false, false, undefined, undefined, null, null, NaN, NaN, 'NaN', 0, 0, 'a', 'a', {}, {}];
console.log(unique1(arrBefore));
//[1, "true", true, 15, false, undefined, null, NaN, "NaN", 0, "a", {}, {}]


// 双for + splice 缺点：NaN和{}没有去重，两个null直接消失了
function unique2(arr) {
    for (let i = 0; i < arr.length; i++) {
        for (let j = i + 1; j < arr.length; i++) {
            if (arr[i] == arr[j]) {
                arr.splice(j, 1);
                j--;
            }
        }
    }
    return arr;
}
//[1, "true", 15, false, undefined, NaN, NaN, "NaN", "a", {…}, {…}]     //NaN和{}没有去重，两个null直接消失了


// 利用indexOf 缺点：不能去重NaN，函数，对象
function unique3(arr) {
    if (!Array.isArray(arr)) {
        console.log('type error!');
        return;
    }
    const res = [];
    for (let i = 0; i < arr.length; i++) {
        if (res.indexOf(arr[i]) === -1) {
            res.push(arr[i]);
        }
    }
    return res;
}
// [1, "true", true, 15, false, undefined, null, NaN, NaN, "NaN", 0, "a", {…}, {…}]  //NaN、{}没有去重



// 利用sort() 排序后去掉前面相同的值
function unique4(arr) {
    if (!Array.isArray(arr)) {
        console.log('type error!');
        return;
    }
    arr = arr.sort();
    let res = [arr[0]];
    for (let i = 1; i < arr.length; i++) {
        if (arr[i] !== arr[i - 1]) {
            res.push(arr[i]);
        }
    }
    return res;
}
// [0, 1, 15, "NaN", NaN, NaN, {…}, {…}, "a", false, null, true, "true", undefined]      //NaN、{}没有去重


// 利用对象的属性不能相同的特点进行去重（这种数组去重的方法有问题，不建议用，有待改进）
function unique5(arr) {
    if (!Array.isArray(arr)) {
        console.log('type error!');
        return;
    }
    let res = [];
    let obj = {};
    for (let i = 0; i < arr.length; i++) {
        if (!obj[arr]) {
            res.push(arr[i]);
            obj[arr[i]] = 1;
        } else {
            obj[arr[i]]++;
        }
    }
    return res;
}
//[1, "true", 15, false, undefined, null, NaN, 0, "a", {…}]    //两个true直接去掉了，NaN和{}去重


// includes
function unique6(arr) {
    if (!Array.isArray(arr)) {
        console.log('type error!');
        return;
    }
    let res = [];
    for (let i = 0; i < arr.length; i++) {
        if (!arr.includes(arr[i])) {
            res.push(arr[i]);
        }
    }
    return res;
}
//[1, "true", true, 15, false, undefined, null, NaN, "NaN", 0, "a", {…}, {…}]     //{}没有去重


// filter + hasOwnProperty
function unique7(arr) {
    let obj = {};
    return arr.filter((item) => {
        return obj.hasOwnProperty(typeof item + JSON.stringify(item)) ? false : (obj[typeof item + JSON.stringify(item)] = true);
    })
}
//[1, "true", true, 15, false, undefined, null, NaN, "NaN", 0, "a", {…}]   // 所有的都去重了*************************************************************************************


// filter 
function unique8() {
    return arr.filter(function(item, index, arr) {
        return arr.indexOf(item, 0) === index;
    })
}
//[1, "true", true, 15, false, undefined, null, "NaN", 0, "a", {…}, {…}] // NaN不见了，对象没办法去重

// 利用递归去重
function unique9() {
    let array = arr;
    let len = array.length;

    array.sort(function(a, b) {
        return a - b;
    })

    function loop(index) {
        if (index >= 1) {
            if (array[index] === array[index - 1]) {
                array.splice(index, 1);
            }
            loop(index - 1);
        }
    }
    loop(len - 1);
    return array;
}
//[1, "a", "true", true, 15, false, 1, {…}, null, NaN, NaN, "NaN", 0, "a", {…}, undefined] // NaN、{}没有去重

// 利用Map数据结构去重
function unique10(arr) {
    let map = new Map();
    let array = new Array();
    for (let i = 0; i < arr.length; i++) {
        if (mp.has(arr[i])) {
            map.set(arr[i], true);
        } else {
            map.set(arr[i], false);
            array.push(arr[i]);
        }
    }
    return array;
}
//[1, "a", "true", true, 15, false, 1, {…}, null, NaN, NaN, "NaN", 0, "a", {…}, undefined] // NaN、{}没有去重


// reduce + includes
function unique11(arr) {
    return arr.reduce((prev, cur) => prev.includes(cur) ? prev : [...prev, cur], []);
}

console.log(unique11(arrBefore));
// [1, "true", true, 15, false, undefined, null, NaN, "NaN", 0, "a", {…}, {…}] // 对象不能去重

// 代码就是这么少----（其实，严格来说并不算是一种，相对于第一种方法来说只是简化了代码）
[...new Set(arr)]