function A(name) {
    this.name = name || 'A';
    this.sayName = function() {
        console.log(this.name);
    }
}
// 1.盗用构造函数方法不能继承父类原型中的方法
// 2.函数不能重用
A.prototype.sayHello = function() {
    console.log('Hello' + this.name);
}

function B(name, age) {
    A.call(this, name);
    this.age = age;
}

B.prototype.sayAge = function() {
    console.log('Age' + this.age);
}

let b = new B('airsage', 23);
b.sayName();
b.sayAge();