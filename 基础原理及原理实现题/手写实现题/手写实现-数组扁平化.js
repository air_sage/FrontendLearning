function checkType(arr) {
    return Object.prototype.toString.call(arr).slice(8,-1);
}

Array.prototype.myFlat = function(num) {
    let type = checkType(this);
    let res = [];
    if(type !== 'Array') {
        return;
    }
    console.log(this);
    this.forEach(item => {
        let cellType = checkType(item);
        // console.log(cellType);
        if(cellType === 'Array') {
            console.log(num);
            num--;
            if(num < 0) {
                let newArr = res.push(item);
                return newArr;
            }
            res.push(...item.myFlat(num));
            num++;
        } else {
            res.push(item);
        }
    })
    return res;
}

let arr1 = [1, [2, 3, [4, 5, [12, 3, "zs"], 7, [8, 9, [10, 11, [1, 2, [3, 4]]]]]]];
let res = arr1.myFlat(3);
console.log(res);
let arr2 = [1, [2, 3, [4, 5, [12, 3, "zs"], 7, [8, 9, [10, 11, [1, 2, [3, 4]]]]]]];
let res1 = arr2.flat(3);
console.log(res1);
