function selectSort(arr) {
    let len = arr.length;
    let current;
    for (let i = 0; i < len; i++) {
        current = arr[i];
        let j = i;
        while (j - 1 >= 0 && current < arr[j - 1]) {
            arr[j] = arr[j - 1];
            j--;
        }
        arr[j] = current;
    }
    return arr;
}

arr1 = [9, 8, 4, 6, 5, 1];
console.log(selectSort(arr1));