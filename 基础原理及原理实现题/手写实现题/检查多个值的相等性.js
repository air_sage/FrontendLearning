function recursivelyCheckEqual(x, ...rest) {
    return Object.is(x, rest[0]) && (rest.length < 2 || recursivelyCheckEqual(...rest));
}

console.log(recursivelyCheckEqual(NaN, NaN, NaN)); // true
console.log(recursivelyCheckEqual(NaN, NaN, NaN, null)); // false
console.log(recursivelyCheckEqual(1, 1, 1, 1)); // true
console.log(recursivelyCheckEqual(1, 1, "1", 1)); // false