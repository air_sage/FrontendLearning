function inheritPrototype(son, father) {
    son.prototype = Object.create(father.prototype);
    son.prototype.constructor = son;
}

function A(name) {
    this.name = name || 'A';
    this.sayName = function() {
        console.log(this.name);
    }
}

A.prototype.sayHello = function() {
    console.log('Hello' + this.name);
}

function B(name, age) {
    A.call(this, name);
    this.age = age;
}

// B.prototype = Object.create(A.prototype);
// B.prototype.constructor = B;
inheritPrototype(B, A);


B.prototype.sayAge = function() {
    console.log('Age' + this.age);
}

let b = new B('airsage', 24);
console.log(b);
b.sayAge();