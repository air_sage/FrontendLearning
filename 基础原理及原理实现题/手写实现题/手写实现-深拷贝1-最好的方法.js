// 考虑循环引用会出现内存溢出的问题，使用Map来解决
// 用weakMap来代替Map可以

function deepCopy(target, map = new WeakMap()) {
    if (target && typeof target === 'object') {
        let copyTarget = Array.isArray(target) ? [] : {}; // 考虑数组和对象两种情况
        if (map.get(target)) {
            return map.get(target);
        }
        map.set(target, copyTarget);
        for (const key in target) {
            copyTarget[key] = deepCopy(target[key], map); //递归实现深拷贝
        }
        return copyTarget;
    } else {
        return target;
    }
}

const target = {
    field1: 1,
    field2: undefined,
    field3: {
        child: 'child'
    },
    field4: [2, 4, 8]
};
target.target = target;

const res = deepCopy(target);
console.log(res);

function deepCopy(target, map = new WeakMap()) {
    if (target && typeof target == 'object') {
        let copyTarget = Array.isArray(target) ? [] : {};
        if (map.get(target)) {
            return map.get(target);
        }
        map.set(target, copyTarget);
        for (const key in target) {
            copyTarget[key] = deepCopy(target[key], map);
        }
        return copyTarget;
    } else {
        return target;
    }
}