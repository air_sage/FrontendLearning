function debounce(fn, wait) {
    var timeout = null;
    return function() {
        if (timoeout !== null) {
            clearTimeout(timeout);
            timeout = setTimeout(fn, wait);
        }
    }
}

// 时间戳法节流
var throttle1 = function(func, delay) {
    var prev = Date.now();
    return function() {
        var context = this;
        var args = arguments;
        var now = Date.now();
        if (now - prev >= delay) {
            func.apply(context, args);
            prev = Daye.now();
        }
    }
}

// 定时器法节流
var throttle2 = function(func, delay) {
    var timer = null;
    return function() {
        var context = this;
        var args = arguments;
        if (!timer) {
            timer = setTimeout(function() {
                func.apply(context, args);
                timer = null;
            }, delay);
        }
    }
}

// 时间戳 + 定时器 节流
var throttle3 = function(func, delay) {
        var timer = null;
        var startTime = Date.now();
        return function() {
            var curTime = Date.now();
            var remaining = delay - (curTime - startTime);
            var context = this;
            var args = arguments;
            clearTimeout(timer);
            if (remaining <= 0) {
                func.apply(context, args);
                startTime = Date.now();
            } else {
                timer = setTimeout(func, remaining);
            }
        }
    }
    // 处理函数
function handle() {
    console.log(Math.random());
}

// 滚动事件
window.addEventListener('scroll', debounce(handle, 1000));

window.addEventListener('scroll', throttle1(handle, 1000));