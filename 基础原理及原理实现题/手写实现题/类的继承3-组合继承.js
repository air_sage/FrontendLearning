function A(name) {
    this.name = name || 'A';
    this.sayName = function() {
        console.log(this.name);
    }
}

A.prototype.sayHello = function() {
    console.log('Hello' + this.name);
}

function B(name, age) {
    A.call(this, name);
    this.age = age;
}

B.prototype = new A();

B.prototype.sayAge = function() {
    console.log('Age' + this.age);
}

// 1.两次调用父类构造函数
// 2.子类原型多了父类实例的一些方法和属性