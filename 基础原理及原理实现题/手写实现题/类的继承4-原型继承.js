function objectCreate(o) {
    let Fn = function() {};
    Fn.prototype = o;
    return new Fn();
}


// 或者使用ES5语法 Object.create();

let person = {
    name: 'airsage',
    friends: ['a', 'b', 'c']
};

let anotherPerson = objectCreate(person);
anotherPerson.name = 'zsz';
anotherPerson.friends.push('d');
console.log(anotherPerson);
console.log(anotherPerson.friends);

let yetAnotherPerson = objectCreate(person);
yetAnotherPerson.name = 'zengshengzhao';
yetAnotherPerson.friends.push('e');
console.log(yetAnotherPerson);
console.log(yetAnotherPerson.friends);

// ES5已经有现成方法Object.create()
let anotherPerson1 = Object.create(person);
let anotherPerson2 = Object.create(person, {
    name: {
        value: 'woshishui'
    }
});

console.log(anotherPerson2.name);

// 缺点：
// 1.只能是实例间的继承
// 2.引用类型数据共享