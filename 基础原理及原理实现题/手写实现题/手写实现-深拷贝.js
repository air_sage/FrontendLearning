function deepCopy(target) {
    if (typeof target === 'object') {
        let copyTarget = Array.isArray(target) ? [] : {}; // 考虑数组和对象两种情况
        for (const key in target) {
            copyTarget[key] = deepCopy(target[key]); //递归实现深拷贝
        }
        return copyTarget;
    } else {
        return target;
    }
}

const target = {
    field1: 1,
    field2: undefined,
    field3: {
        child: 'child'
    },
    field4: [2, 4, 8]
};

const res = deepCopy(target);
console.log(res);