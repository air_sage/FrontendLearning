// var Type = {};

// for (var i = 0, type; type = ['String', 'Number', 'Boolean', 'Array'][i++];) {
//     (function(type) {
//         Type['is' + type] = function(target) {
//             return Object.prototype.toString.call(target) === '[object ' + type + ']';
//         }
//     })(type);

// }



// // console.log(Type);
// console.log(i);
// console.log(Type.isString(""));
// console.log(Type.isString([]));

// // 使用let也是可以的
// for (let i = 0, type; type = ['String', 'Number', 'Boolean', 'Array'][i++];) {
//     Type['is' + type] = function(target) {
//         return Object.prototype.toString.call(target) === '[object ' + type + ']';
//     }

// }

var findRepeatNumber = function(nums) {
    const map = new Map();
    for (let i = 0, item; item = nums[i++];) {
        console.log(item);
        if (map.get(item)) {
            return item;
        } else {
            map.set(item, true);
        }
    }
};

let res = findRepeatNumber([2, 3, 1, 0, 2, 5, 3]);
console.log(res);