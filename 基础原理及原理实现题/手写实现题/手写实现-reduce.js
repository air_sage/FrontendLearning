Array.prototype.myReduce = function(fn, initialVal) {
    const arr = [...this];
    if (!arr.length) {
        return;
    }
    let res;
    if (initialVal) {
        res = initialVal;
        for (let i = 0; i < arr.length; i++) {
            res = fn(res, arr[i], i, this);
        }
    } else {
        res = arr[0];
        for (let i = 1; i < arr.length; i++) {
            res = fn(res, arr[i], i, this);
        }
    }
    return res;
}

const array1 = [1, 2, 3, 4, 5];
const reducer = (accumulator, currentValue) => accumulator + currentValue;
console.log(array1.myReduce(reducer));