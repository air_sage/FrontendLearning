for (var i = 1; i <= 5; i++) {
    setTimeout(function() {
        console.log(i);
    }, 1000 * i);
}

// 方法1
// for (var i = 1; i <= 5; i++) {
//     setTimeout((function(a) {
//         return function() {
//             console.log(a);
//         };
//     })(i), 1000 * i)
// }

// 方法2
// function timer(i) {
//     setTimeout(function() {
//         console.log(i);
//     }, 1000 * i);
// }
// for (var i = 1; i <= 5; i++) {
//     timer(i);
// }

// 方法3
// for (let i = 1; i <= 5; i++) {
//     setTimeout(function() {
//         console.log(i);
//     }, 1000 * i);
// }

// 方法4
// for (var i = 1; i <= 5; i++) {
//     (function(i) {
//         setTimeout(function() {
//             console.log(i);
//         }, 1000 * i)
//     })(i);
// }