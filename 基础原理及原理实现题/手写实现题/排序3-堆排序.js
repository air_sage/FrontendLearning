// 堆排序
function heapSort(arr) {
    for (let i = arr.length - 1; i >= 0; i--) {
        sort(i);
        [arr[i], arr[0]] = [arr[0], arr[i]];
    }

    function sort(i) {
        let p = Math.floor(i / 2 - 1);
        while (p >= 0) {
            let childLeft = 2 * p + 1;
            let childRight = 2 * p + 2;
            let max = childLeft;
            if (childRight <= i && arr[childRight] > arr[childLeft]) {
                max = childRight;
            }
            if (arr[max] > arr[p]) {
                [arr[max], arr[p]] = [arr[p], arr[max]];
            }
            p--;
        }
    }
}

let arr = [7, 8, 5, 4, 3, 9];;
heapSort(arr);
console.log(arr);








// function heapify(arr, n, i) {
//     if (i >= n) return;
//     let childLeft = 2 * i + 1;
//     let childRight = 2 * i + 2;
//     let min = i;
//     if (childLeft < n && arr[childLeft] < arr[min]) min = childLeft;
//     if (childRight < n && arr[childRight] < arr[min]) min = childRight;
//     if (i !== min) {
//         [arr[i], arr[min]] = [arr[min], arr[i]];
//         heapify(arr, n, min);
//     }
// }

// function heapSort(arr) {
//     let n = arr.length;
//     let node = n - 1;
//     let parent = (node - 1) / 2;
//     for (let i = parent; i >= 0; i--) {
//         heapify(arr, n, i);
//     }
//     return arr;
// }


// // 使用栈代替递归方法，防止内存溢出
// const stack = [];

// function heapSort1(arr) {
//     let n = arr.length;
//     let node = n - 1;
//     let parent = (node - 1) / 2;
//     for (let i = parent; i >= 0; i--) {
//         stack.push(i);
//         while (stack.length > 0) {
//             heapify1(arr, n, stack.shift());
//         }
//     }
//     return arr;
// }

// function heapify1(arr, n, i) {
//     if (i >= n) return;
//     let childLeft = 2 * i + 1;
//     let childRight = 2 * i + 2;
//     let min = i;
//     if (childLeft < n && arr[childLeft] < arr[min]) min = childLeft;
//     if (childRight < n && arr[childRight] < arr[min]) min = childRight;
//     if (i !== min) {
//         [arr[i], arr[min]] = [arr[min], arr[i]];
//         stack.push(min);
//         console.log(stack);
//     }
// }

// arr1 = [9, 8, 4, 6, 5, 1];
// console.log(heapSort1(arr1));
// arr2 = [9, 8, 4, 6, 5, 1];
// console.log(heapSort(arr2));


// var len;
// const stack = [];

// function buildHeap(arr) {
//     len = arr.length;
//     for (let i = Math.floor(arr.length / 2); i >= 0; i--) {
//         adjustHeap(arr, i);
//     }
// }

// function adjustHeap(arr, i) {
//     let childLeft = 2 * i + 1;
//     let childRight = 2 * i + 2;
//     let max = i;
//     if (childLeft < len && arr[childLeft] > arr[max]) max = childLeft;
//     if (childRight < len && arr[childRight] > arr[max]) max = childRight;
//     if (i != max) {
//         [arr[i], arr[max]] = [arr[max], arr[i]];
//         adjustHeap(arr, max);
//     }
// }

// function heapSort(arr) {
//     buildHeap(arr);
//     for (let i = arr.length - 1; i > 0; i--) {
//         [arr[0], arr[i]] = [arr[i], arr[0]];
//         len--;
//         adjustHeap(arr, 0);
//     }
//     return arr;
// }


// heapSort(arr1);
// console.log(arr1);

// function heapSort(arr) {
//     for (let i = arr.length - 1; i > 0; i--) {
//         sort(i);
//         [arr[i], arr[0]] = [arr[0], arr[i]];
//     }

//     function sort(i) {
//         let p = Math.floor((i - 1) / 2);
//         while (p >= 0) {
//             let childLeft = 2 * p + 1;
//             let childRight = 2 * p + 2;
//             let max = childLeft;
//             if (childRight <= i && arr[childRight] > arr[childLeft]) {
//                 max = childRight;
//             }
//             if (arr[max] > arr[p]) {
//                 [arr[max], arr[p]] = [arr[p], arr[max]];
//             }
//             p--;
//         }
//     }
// }

// arr1 = [9, 8, 4, 6, 5, 1];

// heapSort(arr1);
// console.log(arr1);