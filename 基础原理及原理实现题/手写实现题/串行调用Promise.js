const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})


let promise1 = function() {
    return new Promise(resolve => {
        readline.question('你叫什么名字？', name => {
            // console.log(name);
            obj.name = name;
            resolve(name + '');
            // readline.close();
        })

    })
}

let promise2 = function(str) {
    return new Promise(resolve => {
        readline.question('你几岁了？', age => {
            // console.log(age);
            obj.age = age;
            resolve(str + ',' + age);
            // readline.close();
        })
    })
}
let promise3 = function(str1) {
    return new Promise(resolve => {
        readline.question('你多少斤？', weight => {
            let a = str1.split(',');
            let name = a[0];
            let age = a[1];
            obj.weight = weight;
            // console.log(weight);
            resolve(name + "," + age + "," + weight);
            readline.close();
        })
    })
}

const arr = [promise1, promise2, promise3];
const obj = {};

function iter(i) {
    if (arr.length) {
        let p = arr.shift();
        p(i).then(data => {
            console.log(data);

            iter(data);
        });
    } else {
        let ar = ['姓名', '年龄', '体重'];
        let a = i.split(',');
        for (let i = 0; i < a.length; i++) {
            console.log(ar[i] + ':' + a[i]);
        }
        console.log(obj);
    }
};
iter();