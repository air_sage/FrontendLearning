const { traceDeprecation } = require("process");

// 插入排序
var insertSort = function(arr) {
    let preIndex = 0;
    for (let i = 0; i < arr.length; i++) {
        let temp = arr[i];
        preIndex = i - 1;
        while (preIndex >= 0 && arr[preIndex] > temp) {
            arr[preIndex + 1] = arr[preIndex];
            preIndex -= 1;
        }
        arr[preIndex + 1] = temp;
    }
    return arr;
}

var arrBefore = [5, 6, 4, 9, 8, 1];
let arr1 = insertSort([5, 6, 4, 9, 8, 1]);
console.log(arr1);

// 希尔排序
var shellSort = function(arr) {
    var len = arr.length,
        temp, gap = 1;
    let preIndex = 0;
    while (gap < len / 3) {
        gap = gap * 3 + 1;
    }
    for (gap; gap > 0; gap = Math.floor(gap / 3)) {
        for (var i = 0; i < len; i++) {
            temp = arr[i];
            preIndex = i - gap;
            while (preIndex >= 0 && arr[preIndex] > temp) {
                arr[preIndex + gap] = arr[preIndex];
                preIndex -= gap;
            }
            arr[preIndex + gap] = temp;
        }
    }
    return arr;
}

// 归并排序
var merge = function(left, right) {
    var result = [];
    while (left.length && right.length) {
        if (left[0] > right[0]) {
            result.push(right.shift());
        } else {
            result.push(left.shift());
        }
    }
    return result.concat(right, left);
}


var mergeSort = function(arr) {
    if (arr.length <= 1) return arr;
    let mid = Math.floor(arr.length / 2);
    let left = arr.slice(0, mid);
    let right = arr.slice(mid);
    return merge(mergeSort(left), mergeSort(right));
}

console.log(mergeSort(arrBefore));


// 桶排序/基数排序
var bucketSort = function(arr) {
    let min = arr[0];
    let max = arr[0];
    let len = arr.length;
    let sum = len;
    let size = 0;
    let index;
    for (let i = 1; i < len; i++) {
        if (arr[i] > max) {
            max = arr[i];
        } else if (arr[i] < min) {
            min = arr[i];
        }
    }

    if (max != min) {
        var bucket = new Array(len);
        for (let i = 0; i < sum; i++) {
            index = Math.floor((arr[i] - min) / (max - min) * (sum - 1));
            bucket[index] = new Array()
            bucket[index].push(arr[i]);
        }
        for (let i = 0; i < sum; i++) {
            if (bucket[i].length > 1) {
                bucketSort(bucket[i]);
            }
            for (let j = 0; j < bucket.length; j++) {
                arr[size++] = bucket[i][j];
            }
        }
    }
    return arr;
}

console.log(bucketSort([5, 6, 4, 9, 8, 1]));

// 堆排序
var heapify = function(tree, n, i) {
    if (i >= n) {
        return;
    }
    let child1 = 2 * i + 1;
    let child2 = 2 * i + 2;
    let max = i;
    // 小顶堆两个小于号；大顶堆两个大于号
    if (child1 < n && tree[child1] < tree[max]) {
        max = child1;
    }
    if (child2 < n && tree[child2] < tree[max]) {
        max = child2;
    }
    if (max != i) {
        [tree[i], tree[max]] = [tree[max], tree[i]];
        heapify(tree, n, max);
    }
}

var heapSort = function(tree) {
    // n是数组长度
    let n = tree.length;
    let lastNode = n - 1;
    let parent = (lastNode - 1) / 2;
    let i;
    for (i = parent; i >= 0; i--) {

        heapify(tree, n, i);
    }
    return tree;
}

let arrTree = [5, 6, 4, 9, 8, 1];
console.log(heapSort(arrTree));
console.log(arrTree);


// 快速排序
var quickSort = function(arr, left, right) {
    let target = arr[left];
    let i = left,
        j = right;
    if (left > right) {
        return;
    }
    while (i !== j) {
        while (i < j && arr[j] >= target) {
            j--;
        }
        while (i < j && arr[i] < target) {
            i++;
        }
        if (i < j) {
            [arr[i], arr[j]] = [arr[j], arr[i]];
        }
    }
    // arr[left] = arr[i];
    // arr[i] = target;
    [arr[left], arr[i]] = [arr[i], arr[left]];
    quickSort(arr, left, i - 1);
    quickSort(arr, i + 1, right);
    return arr;
}

console.log(quickSort([5, 6, 4, 9, 8, 1], 0, 5));