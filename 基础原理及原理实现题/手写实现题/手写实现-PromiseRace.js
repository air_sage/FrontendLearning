Promise.myRace = function(iterator) {
    return new Promise((resolve, reject) => {
        for (let item of iterator) {
            Promise.resolve(item).then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            })
        }
    })
}

let p1 = new Promise(resolve => {
    setTimeout(resolve, 105, 'p1 done')
})
let p2 = new Promise((resolve, reject) => {
    setTimeout(reject, 111, 'p2 done')
})
let p3 = Promise.resolve(3)


Promise.myRace([p1, p2, p3]).then(data => {
    console.log(data);
})