/**
 * 将函数柯里化
 * @param fn    待柯里化的原函数
 * @param len   所需的参数个数，默认为原函数的形参个数
 */
/**
 * 中转函数
 * @param fn    待柯里化的原函数
 * @param len   所需的参数个数
 * @param args  已接收的参数列表
 */

// function curry(fn, len = fn.length) {
//     return _curry.call(this, fn, len);
// }

// function _curry(fn, len, ...arg) {
//     return function(...params) {
//         let _arg = [...arg, ...params];
//         if (_arg.length >= len) {
//             return fn.apply(this, _arg);
//         } else {
//             return _curry.call(this, fn, len, ..._arg);
//         }
//     }
// }

// var arr = [1, 2, 3, 4, 5];

// function solve() {
//     arr = [1, 2, 3, 4];
// }
// console.log(arr);

function curry(fn, args) {
    let len = fn.length;
    args = args || [];
    return function() {
        let newArgs = args.concat(Array.from(arguments));
        if (newArgs.length < len) {
            return curry.call(this, fn, newArgs);
        } else {
            return fn.apply(this, newArgs);
        }
    }
}

function solve(a, b, c) {
    return a + b + c;
}
let add = curry(solve);

let add1 = add(1)(2);

console.log(add1(1));
console.log(add(1)(2)(3));

// function curry(fn, args) {
//     let len = fn.length;
//     args = args || [];
//     return function() {
//         let 
//     }
// }