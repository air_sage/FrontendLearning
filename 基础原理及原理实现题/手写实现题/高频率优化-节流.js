// 用flag

const throttle = (fn, delay = 500) => {
    let flag = true;
    return (...args) => {
        if (!flag) return;
        flag = false;
        setTimeout(() => {
            fn.apply(this, args);
            flag = true;
        }, delay);
    };
};

function f(n) {

}

// 用timer
function throttle1(fn, delay = 500) {
    var timer;
    return function() {
        var context = this;
        var args = arguments;
        if (timer) return;
        timer = setTimeout(function() {
            fn.apply(context, args);
            timer = null;
        }, delay);
    }
}

// ES6
const throttle2 = (fn, delay = 500) => {
    let timer;
    return (...args) => {
        if (timer) return;
        timer = setTimeout(() => {
            fn.apply(this, args);
            timer = null;
        }, delay);
    }
}

const throttle3 = (fn, delay = 500) => {
    let timer;
    return (...args) => {
        if (timer) return;
        timer = setTimeout(() => {
            fn.call(this, ...args);
            clearTimeout(timer);
        }, delay)
    }
}