let arr = [1, 3, 4, 5, 7];
// typeof 是没办法判断数组类型的
console.log(typeof arr); // object

// 法一 instanceof
console.log(arr instanceof Array); // true

// 法二 Object.prototype.toString.call()
let type1 = Object.prototype.toString.call(arr); // [object Array]
console.log(type1);

// 法三 构造函数.prototype.isPrototypeOf() 最佳方法
let type2 = Array.prototype.isPrototypeOf(arr); // [object Array]
console.log(type1);

let obj = {};
let type3 = Array.prototype.isPrototypeOf(obj); // 如果是的话返回类型，不是返回false
console.log(type3);

// 法四 Array.isArray() (ES5)
let type4 = Array.isArray(arr); // true
console.log(type4);