// // 1.按顺序的函数柯里化
function curry(fn, args) {
    args = args || [];
    let length = fn.length;
    return function() {
        let _arg = args.slice(0),
            arg, i;
        for (i = 0; i < arguments.length; i++) {
            arg = arguments[i];
            _arg.push(arg);
        }
        if (_arg.length < length) {
            return curry.call(this, fn, _arg);
        } else {
            return fn.apply(this, _arg);
        }
    }
}

var fn = curry(function(a, b, c) {
    console.log([a, b, c]);
});

fn("a", "b", "c") // ["a", "b", "c"]
fn("a", "b")("c") // ["a", "b", "c"]
fn("a")("b")("c") // ["a", "b", "c"]
fn("a")("b", "c") // ["a", "b", "c"]

// 2.允许有占位符的函数柯里化
// function curry(fn, args, holes) {
//     length = fn.length;
//     args = args || [];
//     holes = holes || [];
//     return function() {
//         let _args = args.slice(0),
//             _holes = holes.slice(0),
//             argsLen = args.length,
//             holesLen = holes.length,
//             arg, i, index = 0;
//         for (i = 0; i < arguments.length; i++) {
//             arg = arguments[i];
//             if (arg === _ && holesLen) {
//                 index++;
//                 if (index > holesLen) {
//                     _args.push(arg);
//                     _holes.push(argsLen - 1 + index - holesLen);
//                 }
//             } else if (arg === _) {
//                 _args.push(arg);
//                 _holes.push(argsLen + i);
//             } else if (holesLen) {
//                 if (index >= holesLen) {
//                     _args.push(arg);
//                 } else {
//                     _args.splice(_holes[index], 1, arg);
//                     _holes.splice(index, 1);
//                 }
//             } else {
//                 _args.push(arg);
//             }
//         }
//         if (_holes.length || _args.length < length) {
//             return curry.call(this, fn, _args, _holes);
//         } else {
//             return fn.apply(this, _args);
//         }
//     }
// }

// var _ = {};

// var fn = curry(function(a, b, c, d, e) {
//     console.log([a, b, c, d, e]);
// });

// // 验证 输出全部都是 [1, 2, 3, 4, 5]
// fn(1, 2, 3, 4, 5);
// fn(_, 2, 3, 4, 5)(1);
// fn(1, _, 3, 4, 5)(2);
// fn(1, _, 3)(_, 4)(2)(5);
// fn(1, _, _, 4)(_, 3)(2)(5);
// fn(_, 2)(_, _, 4)(1)(3)(5);

const dp = [0, 1, 1];

function solve(n) {
    if(dp[n]) return dp[n];
    else {
        let res = solve(n - 1) + solve(n - 2);
        dp[n] = res;
        return res;
    }
    
}