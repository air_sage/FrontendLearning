function object(o) {
    function F() {};
    F.prototype = o;
    return new F();
}

// 类似于Object.create()