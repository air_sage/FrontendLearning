var bubbleSort = function(arr) {
    for (let i = 0; i < arr.length; i++) {
        for (let j = i; j < arr.length; j++) {
            if (arr[j] < arr[i]) {
                [arr[i], arr[j]] = [arr[j], arr[i]];
            }
        }
    }
    return arr;
}



// 冒泡优化
function bubbleSort2(arr) {
    for (let i = 0; i < arr.length; i++) {
        let isSort = true;
        for (let j = i; j < arr.length; j++) {
            if (arr[j] < arr[i]) {
                [arr[i], arr[j]] = [arr[j], arr[i]];
                isSort = false;
            }
        }
        if (isSort) {
            break;
        }
    }
    return arr;
}

arr1 = [9, 8, 4, 6, 5, 1];
console.log(bubbleSort2(arr1));