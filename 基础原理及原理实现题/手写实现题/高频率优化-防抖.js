// 函数无形参
function debounce(fn, delay) {
    let timer;
    clearTimeout(timer);
    timer = setTimeout(function() {
        fn();
    }, delay);
}

// 函数有形参
function debounce1(fn, delay) {
    let timer;
    return function(...args) {
        let context = this;
        timer && clearTimeout(timer);
        setTimeout(function() {
            fn.apply(context, args);
        }, delay);
    }
}

// 函数有形参ES6写法
var debounce2 = (fn, delay) => {
    let timer;
    return (...args) => {
        timer && clearTimeout(timer);
        timer = setTimeout(() => {
            fn.apply(this, args);
        }, delay);
    }
}

var debounce = (fn, delay) => {
    let timer;
    return (...args) => {
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(() => {
            fn.apply(this, args);
            clearTimeout(timer);
        }, delay);
    }
}


// 百度-输出判断题
// let db = debounce(function() {
//     console.log('debounced');
// }, 5000)

// let button = document.getElementById("#xx");

// // 使用同一个防抖函数，下面两个最终只会触发一次
// button.onclick(db);
// button.onmousemove(db);

// // 使用不同的防抖函数，会创建两个闭包，所以最终会触发两次
// button.onclick(debounce(function() {
//     console.log('cilck');
// }));

// button.onmousemove(debounce(function() {
//     console.log('click');
// }))