function sqrtNewton(n) {
    if (n < 0) return NaN;
    if (n == 0 || n == 1) return n;
    let val = n;
    let last;
    do {
        console.log(val, last);
        last = val;
        val = (val + n / val) / 2;
    }
    while (Math.abs(val - last) >= Number.EPSILON)
    return val;
}

sqrtNewton(8);