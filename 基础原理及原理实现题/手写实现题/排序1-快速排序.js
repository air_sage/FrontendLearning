// 递归方式实现快速排序
var quickSort = function(arr, left, right) {
    let i = left;
    let j = right;
    let target = arr[left];
    if (left > right) {
        return;
    }
    while (i !== j) {
        while (i < j && arr[j] >= target) {
            j--;
        }
        while (i < j && arr[i] <= target) {
            i++;
        }
        if (i < j) {
            [arr[i], arr[j]] = [arr[j], arr[i]];
        }
    }
    // [arr[left], arr[i]] = [arr[i], arr[left]];
    arr[left] = arr[i];
    arr[i] = target;
    quickSort(arr, left, i - 1);
    quickSort(arr, i + 1, right);
    return arr;
}

// 非递归方法实现快速排序

// 当数据量很大的时候，递归快排会造成栈溢出，为解决此问题，
// 我们可使用js数组来模拟栈，将待排序数组的[left,right]保存到数组中，循环取出进行快排

function quickSort1(arr, left, right) {
    const stack = [
        [left, right]
    ];
    while (stack.length > 0) {
        // 利用解构取出栈中的一个数组元素的两个值
        let [i, j] = stack.shift();
        if (i >= j) {
            continue;
        }
        // 把起始位置保存下来
        let start = i;
        let end = j;
        // 把开始位置的值作为比较的目标
        let target = arr[start];
        while (i !== j) {
            while (i < j && arr[j] >= target) {
                j--;
            }
            while (i < j && arr[i] <= target) {
                i++;
            }
            if (i < j) {
                [arr[i], arr[j]] = [arr[j], arr[i]];
            }
        }
        [arr[start], arr[i]] = [arr[i], arr[start]];

        stack.push([start, i - 1]);
        stack.push([i + 1, end]);
    }
}

arr1 = [9, 8, 4, 6, 5, 1];
quickSort1(arr1, 0, arr1.length - 1);
console.log(arr1);