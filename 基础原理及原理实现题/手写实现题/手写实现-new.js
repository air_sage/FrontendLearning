function myNew(Fn, ...args) {
    let obj = {};
    obj.__proto__ = Fn.prototype;
    obj.__proto__.constructor = Fn;
    let result = Fn.apply(obj, args);
    if (typeof result === 'object') {
        return result;
    }
    // __proto__是一个指针
    return obj;
}

function myNew(Fn, ...args) {
    let obj = {};
    obj.__proto__ = Fn.prototype;
    obj.__proto__.constructor = Fn;
    let result = Fn.apply(obj, args);
    if(typeof result === 'object') {
        return result;
    }
}