function add1() {   // 第一次执行时，定义一个数组专门用来存储所有的参数
    // var args = Array.prototype.slice.call(arguments);
    // var args = [...arguments];

    var args = Array.from(arguments);

    // 在内部声明一个函数，利用闭包的特性保存args并收集所有的参数值

    var adder = function() {     // push() 方法将一个或多个元素添加到数组的末尾
        args.push(...arguments);
        return adder;
    };

    // 利用toString隐式转换的特性，当最后执行时隐式转换，并计算最终的值返回
    // 为adder函数重写他的toString方法
    // 在打印的瞬间调用toString方法

    adder.toString = function() {     // reduce函数，a是之前元素的累加和，b是当前元素，
            return args.reduce(function(a, b) { return a + b; });
        }   // adder.value = function() {
        //   // reduce函数，a是之前元素的累加和，b是当前元素，
        //   return args.reduce(function(a, b) {
        //     return a + b;
        //   });
        // }

    return adder;
}
// console.log(typeof add1(1)(2)(3)); // function
// console.log(add1(1)(2)(3));       // 6
// console.log(add1(1, 2, 3)(4));    // 10
// console.log(add1(1)(2)(3)(4)(5));   // 15
// console.log(add1(2, 6)(1));      // 9
// console.log(add1(2, 6)(1, 3, 4));
// console.log(add1(1)(2, 3)(4).value());


function curry() {
    let arg = Array.from(arguments);
    var adder = function() {
        arg.push(...arguments);
        return adder;
    }

    adder.toString = function() {
        return arg.reduce((a, b) => a + b);
    }
    return adder;
}
console.log(curry(1)(2)(3)); 