var name = 'window1';

var A = {
    name: 'A',
    sayHello: function() {
        var s = () => console.log(this.name);
        return s;
    }
};

var sayHello = A.sayHello();
sayHello();
var B = {
    name: 'B',
}
sayHello.call(B);
sayHello.call();


// var A = {
//     name: 'A',
//     sayHello: () => {
//         console.log(this.name);
//     }
// };

// A.sayHello();