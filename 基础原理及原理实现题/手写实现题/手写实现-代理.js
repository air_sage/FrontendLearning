function myObserve(data) {
    const that = this;
    let handler = {
        get(target, property) {
            return target[property];
        },
        set(target, key, value) {
            let res = Reflect.set(target, key, value);
            that.subscribe[key].map(item => {
                item.update();
            });
            return res;
        }
    }
    this.$data = new Proxy(data, handler);
}

// 记得带热水袋