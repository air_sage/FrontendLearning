// 深拷贝
function deepCopy(object) {
    if (!object || typeof object !== 'object') return object;
    let newobject = Array.isArray(object) ? [] : {};
    for (let key in object) {
        if (object.hasOwnProperty(key)) {
            newobject[key] = deepCopy(object[key]);
        }
    }
    return newobject;
}

// 浅拷贝
function shallowCopy(object) {
    if (!object || typeof object !== 'object') return object;
    let newobject = Array.isArray(object) ? [] : {};
    for (let key in object) {
        if ((object.hasOwnProperty(key))) {
            newobject[key] = object[key];
        }
    }
    return newobject;
}