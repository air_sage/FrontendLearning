const PENDING = "pending";
const FULFILLED = "fullilled";
const REJECTED = "rejected";

function MyPromise(executor) {
    this.status = PENDING;
    this.value = undefined;
    this.reason = undefined;
    this.onResolvedCallbacks = [];
    this.onRejectedCallbacks = [];

    let resolve = (value) => {
        if (value instanceof MyPromise) {
            return value.then(resolve, reject);
        }
        setTimeout(() => {
            if (this.status === PENDING) {
                this.status = FULFILLED;
                this.value = value;
                this.onResolvedCallbacks.forEach(fn => {
                    fn(value);
                })
            }
        }, 0);
    }

    let reject = (reason) => {
        setTimeout(() => {
            if (this.status === PENDING) {
                this.status = REJECTED;
                this.reason = reason;
                this.onRejectedCallbacks.forEach(fn => {
                    fn(reason);
                });
            }
        }, 0);
    }
    try {
        executor(resolve, reject);
    } catch (err) {
        reject(err);
    }
}


MyPromise.prototype.then = function(onFulfilled, onRejected) {
    onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : function(value) {
        return value;
    }

    onRejected = typeof onRejected === 'function' ? onRejected : function(err) {
        return err;
    }

    if (this.status === FULFILLED) {
        onFulfilled(this.value);
    }

    if (this.status === REJECTED) {
        onRejected(this.reason);
    }

    if (this.status === PENDING) {
        this.onResolvedCallbacks.push(onFulfilled);
        this.onRejectedCallbacks.push(onRejected);
    }
}