// Function.prototype.call()
Function.prototype.myCall = function(context) {
    if (typeof this !== "function") {
        console.log("type error");
    }
    let args = [...arguments].slice(1);
    let result = null;
    context = context || this;
    context.fn = this;
    result = context.fn(...args);
    delete context.fn
    return result;
};

// Function.prototype.apply()
Function.prototype.myApply = function(context) {
    if (typeof this !== "function") {
        throw new TypeError("Error");
    }
    let result = null;
    context = context || this;
    context.fn = this;
    if (arguments[1]) {
        result = context.fn(...arguments[1]);
    } else {
        result = context.fn();
    }
    delete context.fn;
    return result;
};

// Function.prototype.bind()
Function.prototype.myBind = function(context) {
    if (typeof this !== "function") {
        throw new TypeError("Error");
    }
    var args = [...arguments].slice(1);
    fn = this;
    return function Fn() {
        return fn.apply(
            this instanceof Fn ? this : context,
            args.concat(...arguments)
        );
    };
};