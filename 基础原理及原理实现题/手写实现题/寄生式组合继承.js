// 引用类型继承的最佳模式

// 组合继承
// function SuperType(name) {
//     this.name = name;
//     this.color = ["red", "blue", "green"];
// }

// SuperType.prototype.sayName = function() {
//     console.log(this.name);T
// }

// function SubType(name, age) {
//     SuperType.call(this, name); // 盗用构造函数 第二次调用SuperType()
//     this.age = age;
// }

// SubType.prototype = new SuperType(); // 第一次调用SuperType()
// SubType.prototype.constructor = SubType;
// SubType.prototype.sayAge = function() {
//     console.log(this.age);
// }


// 原型式继承
// function object(o) {
//     function F() {};
//     F.prototype = o;
//     // 注意：此处返回的的是一个F的实例
//     return new F();
// };

// let person = {
//     name: "airsage",
//     friends: ["a", "b", "c"]
// };

// let anotherPerson = object(person);
// let anotherPerson1 = Object.create(person); // 与上面一句等价
// anotherPerson.name = "zsz";
// anotherPerson.friends.push("d");
// // 原型式继承的引用属性会修改到原型对象的值
// console.log(anotherPerson.friends); // [ 'a', 'b', 'c', 'd' ]
// console.log(person.friends); // [ 'a', 'b', 'c', 'd' ]
// console.log(anotherPerson1.friends); // [ 'a', 'b', 'c', 'd' ]


// 寄生式继承(寄生构造函数和工厂模式) 
// 不关注类型和构造函数，只关注对象
// function object(o) {
//     function F() {};
//     F.prototype = o;
//     return new F(); // 注意：此处返回的的是一个F的实例
// };

// function createAnother(original) {
//     let clone = object(original); // object不是必须，任何返回新对象的函数都可以
//     clone.sayHi = function() {
//         console.log("hi");
//     };
//     return clone;
// };

// let person = {
//     name: "airsage",
//     friends: ["a", "b", "c"]
// };

// let anotherPerson = createAnother(person);
// anotherPerson.sayHi();


// 寄生式组合继承
// function SuperType(name) {
//     this.name = name;
//     this.color = ["red", "blue", "green"];
// }

// SuperType.prototype.sayName = function() {
//     console.log(this.name);
// }

// function SubType(name, age) {
//     SuperType.call(this, name);
//     this.age = age;
// }

// function inheritPrototype(subType, superType) {
//     let prototype = object(superType.prototype);
//     prototype.constructor = subType;
//     subType.prototype = prototype;
// }
// inheritPrototype(SubType, SuperType);

// SubType.prototype.sayAge = function() {
//     console.log(this.age);
// }

// let sType = new SubType("airsage", 23);
// sType.sayName();
// sType.sayAge();