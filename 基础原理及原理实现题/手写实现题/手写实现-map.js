Array.prototype.myMap = function(fn, thisValue) {
    let res = [];
    thisValue = thisValue || [];
    let arr = this;
    for (let i = 0; i < arr.length; i++) {
        res.push(fn.call(thisValue, arr[i], i, arr));
    }
    return res;
}

const a = [1, 2, 3];
const b = a.myMap((item, index) => {
    return item + 1;
})
console.log(b);



// Array.prototype.myReduce = function(fn, thisValue) {

// }

let num = Number(1);
console.log(num)