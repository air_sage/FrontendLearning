// 1.原型链
// 缺点：
// 1.引用类型数据会共享 
// 2.子类实例化时不能给父类型传参数 
// 3.父类的实例属性和方法会变为子类的原型属性和方法
function Father() {
    this.property = true;
}

Father.prototype.getFatherValue = function() {
    return this.property;
}

function Son() {
    this.sonProperty = false;
}

Son.prototype = new Father();

Son.prototype.getSonValue = function() {
    return this.sonProperty;
}

let instance1 = new Son();
console.log(instance.getFatherValue());


// 2.借用构造函数法
// 缺点：
// 1.函数不能重用
// 2.子类不能访问父类原型上定义的方法，因此所有类型只能用构造函数模式
function Father(name) {
    this.name = name;
}

function Son() {
    // 利用call或者assign来调用父级构造函数
    Father.call(this, "airsage");
    // 实例属性
    this.age = 29;
}

let instance2 = new Son();
console.log(instance.name);
console.log(instance.age);


// 3.组合继承(结合原型链和构造函数法的优点)
// 缺点：1.调用了两次超类的构造函数，造成了子类型的原型中多了很多不必要的属性
function Father(name) {
    this.name = name;
    this.colors = ['red', 'blue', 'green'];
}

Father.prototype.sayName = function() {
    console.log(this.name);
}

function Son(name, age) {
    Father.call(this, name);
    this.age = age;
}

Son.prototype = new Father();

Son.prototype.sayAge = function() {
    console.log(this.age);
}

// let instance3 = new Son("airage", 23);
// instance3.colors.push('black');
// console.log(instance3.colors);
// instance3.sayName();
// instance3.sayAge();

// let instance4 = new Son("zsz", 18);
// instance4.colors.push('grey');
// console.log(instance4.colors);
// instance4.sayName();
// instance4.sayAge();

// 结果：
// [ 'red', 'blue', 'green', 'black' ]
// airage
// 23
// [ 'red', 'blue', 'green', 'grey' ]
// zsz
// 18


// 4.原型式继承(针对实例对象)
function object(o) {
    function F() {};
    F.prototype = o;
    return new F();
}

let person = {
    name: 'airsage',
    friends: ['a', 'b', 'c']
};

let anotherPerson = object(person);
anotherPerson.name = 'zsz';
anotherPerson.friends.push('d');

let yetAnotherPerson = object(person);
yetAnotherPerson.name = 'zengshengzhao';
yetAnotherPerson.friends.push('e');

console.log(person.friends);

// ES5已经有现成方法Object.create()
let anotherPerson1 = Object.create(person);
let anotherPerson2 = Object.create(person, {
    name: {
        value: 'woshishui'
    }
});

console.log(anotherPerson2.name);



// 5.寄生式继承(类似于寄生构造函数和工厂模式)
function object(o) {
    function F() {};
    F.prototype = o;
    return new F();
}

function createAnother(original) {
    let clone = object(original);
    clone.sayHi = function() {
        console.log('hi');
    };
    return clone;
}

let person3 = {
    name: 'airsage',
    friends: ['a', 'b', 'c']
};

let anotherPerson3 = createAnother(person3);
anotherPerson3.sayHi();

// 缺点：1.函数难以重用


// 6.寄生组合继承(解决组合继承中二次调用父级构造函数的问题)

function Father(name) {
    this.name = name;
    this.colors = ['red', 'blue', 'green'];
}

Father.prototype.sayName = function() {
    console.log(this.name);
}

function Son(name, age) {
    Father.call(this, name);
    this.age = age;
}

// inheritPrototype 写法一
// function inheritPrototype(son, father) {
//     let prototype = object(father.prototype);
//     prototype.constructor = son;
//     son.prototype = prototype;
// }
// inheritPrototype(Son, Father);
// Son.prototype = Object.create(Father.prototype);
// Son.prototype.constructor = Son;

// inheritPrototype 写法二
function inheritPrototype(son, father) {
    son.prototype = Object.create(father.prototype);
    son.prototype.constructor = son;
}
inheritPrototype(Son, Father);

Son.prototype.sayAge = function() {
    console.log(this.age);
}