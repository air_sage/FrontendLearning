// 在原型继承基础上进行数据增强
function createObject(o) {
    var Fn = function() {};
    Fn.prototype = o;
    return new Fn();
}

function createAnother(origin) {
    let clone = createObject(origin);
    clone.sayHi = function() {
        console.log('hi');
    };
    return clone;
}

let person = {
    name: 'airsage',
    friends: ['a', 'b', 'c']
};

let anotherPerson = createAnother(person);
anotherPerson.sayHi();

// 缺点：
// 1.函数不能重用
// 2.针对实例之间的继承