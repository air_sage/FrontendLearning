// 手写发布订阅
class EventEmitter {
    constructor() {
        this.event = {};
    }
    on(eventName, callback) {
        if (this.event[eventName]) {
            this.event[eventName] = [callback];
        }
        this.event[eventName].push(callback);

    }
    emit(eventName) {
        if (this.event[eventName]) {
            this.event[eventName].forEach(cb => {
                cb();
            })
        }
    }
    removeListener(eventName, callback) {
        if (this.event[eventName]) {
            this.event[eventName] = this.event[eventName].filter(cb => cb != callback);
        }
    }
    once(eventName, callback) {
        let fn = () => {
            callback();
            this.removeListener(eventName);
        }
        this.on(eventName, fn);
    }
}


// 手写PromiseAll
function myPromiseAll(args) {
    return new Promise((resolve, reject) => {
        let count = 0;
        const arr = [];
        for (let i = 0; i < args.length; i++) {
            Promise.resolve(args[i]).then(res => {
                arr[i] = res;
                count++;
                if (count == args.length) {
                    resolve(arr);
                }
            }).catch(err => {
                reject(err);
            })
        }
        if (count == 0) {
            resolve([]);
        }
    })
}

// 手写堆排序
function heapSort(arr) {
    for (let i = arr.length - 1; i > 0; i--) {
        sort(i);
        [arr[i], arr[0]] = [arr[0], arr[i]];
    }

    function sort(i) {
        let p = Math.floor(i / 2 - 1);
        while (p >= 0) {
            let childLeft = 2 * p + 1;
            let childRight = 2 * p + 2;
            let max = childLeft;
            if (childRight <= i && arr[childRight] > arr[childLeft]) {
                max = childRight;
            }
            if (arr[max] > arr[p]) {
                [arr[max], arr[p]] = [arr[p], arr[max]];
            }
            p--;
        }
    }

}


let arr = [7, 8, 5, 4, 7, 3, 7, 4, 9];
heapSort(arr);
console.log(arr);

// 手写深拷贝
function deepCopy(target, map = new WeakMap()) {
    if (typeof target === 'object') {
        let clone = Array.isArray(target) ? [] : {};
        if (map.get(target)) {
            return map.get(target);
        }
        map.set(target, clone);
        for (const key in target) {
            clone[key] = deepCopy(target[key], map);
        }
        return clone;
    } else {
        return target;
    }
}

function instanceOf(target, Fn) {
    while (target.__proto__) {
        if (target.__proto__ === Fn.prototype) {
            return true;
        }
        target = target.__proto__;
    }
    return false;
}


// 手写Promise
function MyPromise(constructor) {
    let self = this;
    self.status = "pending";
    self.value = undefined;
    self.reason = undefined;

    function resolve(value) {
        self.value = value;
        self.status = "resolved";
    }

    function reject(reason) {
        self.reason = reason;
        self.status = "rejected";
    }
    try {
        constructor(resolve, reject);
    } catch (e) {
        reject(e);
    }
}

MyPromise.prototype.then = function(onFullfilled, onRejected) {
    let self = this;
    switch (self.status) {
        case "resovled":
            onFullfilled(self.value);
            break;
        case "resovled":
            onRejected(self.reason);
            break;
    }
}

class MyPromise {
    constructor(executor) {
        this.status = "pending";
        this.value = undefined;
        this.reason = undefined;
        let resolve = (value) => {
            if (this.status === "pending") {
                this.status = "resolved";
                this.value = value;
            }
        }
        let reject = (reason) => {
            if (this.status === "pending") {
                this.status = "rejected";
                this.value = reason;
            }
        }
        try {
            executor(resolve, reject);
        } catch (err) {
            reject(err);
        }
    }
    then(onFullfilled, onRejected) {
        if (this.status === "resolved") {
            onFullfilled(this.value);
        }
        if (this.status === "rejected") {
            onRejected(this.reason);
        }
    }
}