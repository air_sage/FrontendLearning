function A() {
    this.name = 'A';
}
A.prototype.sayHello = function() {
    console.log('Hello' + this.name);
}

function B(name) {
    this.name = name;
}

B.prototype = new A();

let b = new B('airsage');

// 缺点：
// 1.所有的实例共享引用类型
// 2.所有父类实例中的方法和属性全部继承到了子类的原型中
// 3.子类实例化时不能给父类传值