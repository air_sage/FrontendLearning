// 1.可迭代的map
let outputOverN = function(n) {
    let res = [];
    let mp = new Map();
    for (let i = 0; i < this.length; i++) {
        if (mp.has(this[i])) {
            mp.set(this[i], mp.get(this[i]) + 1);
        } else mp.set(this[i], 1);
    }
    // mp["1"] = 3;
    console.log(mp instanceof Map)
    console.log(mp);

    // mp.forEach(function(value, key) {
    //     console.log(key, value);
    //     if (value > n) {
    //         res.push(key);
    //     }
    // })

    for (let [key, value] of mp) {
        if (value > n) {
            res.push(key);
        }
    }
    return res;
}

// 2.不可迭代的map
// let outputOverN = function(n) {
//     let res = [];
//     let mp = new Map();
//     for (let i = 0; i < this.length; i++) {
//         if (mp[this[i]]) {
//             mp[this[i]]++;
//         } else mp[this[i]] = 1;
//     }
//     console.log(mp);
//     for (let key in mp) {
//         if (mp[key] > n) res.push(parseInt(key));
//     }
//     return res;
// }

Array.prototype.outputOverN = outputOverN;

let array = [4, 5, 6, 7, 1, 5, 4, 45, 5, 5, 4, 4, 47, 8, 8, 8];
let result = array.outputOverN(2);
console.log(result);