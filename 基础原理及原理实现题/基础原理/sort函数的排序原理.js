// 注意sort不写参数的时候是字符排序
let values = [0, 1, 5, 10, 15];

values.sort();
console.log(values); // [0, 1, 10, 15, 5]

values.sort((a, b) => a - b);
console.log(values); // [0, 1, 5, 10, 15]