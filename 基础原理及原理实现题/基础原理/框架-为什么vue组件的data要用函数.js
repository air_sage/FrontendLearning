// var Component = function() {};
// Component.prototype.data = {
//     message: '10'
// }
// var component1 = new Component(),
//     component2 = new Component();
// component1.data.message = '20';
// console.log(component2.data.message);  // 20

// // 上面原因是因为component1和component2的属性data引用的是同一个内存地址



var Component = function() {
    this.data = this.data()
}
Component.prototype.data = function() {
    return {
        message: '10'
    }
}
var component1 = new Component(),
    component2 = new Component();
component1.data.message = '20';
console.log(component2.data.message); // 10

// 为什么使用函数不会有这种问题
// 使用函数后，使用的是data()函数，data()函数中的this指向的是当前实例本身。