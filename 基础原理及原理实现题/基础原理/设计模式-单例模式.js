// 透明的单例模式(利用闭包)
// var CreateDiv = (function() {
//     var instance;
//     var CreateDiv = function(html) {
//         if (instance) {
//             return instance;
//         }
//         this.html = html;
//         this.init();
//         return instance = this;
//     };
//     CreateDiv.prototype.init = function() {
//         var div = document.createElement("div");
//         div.innerHTML = this.html;
//         document.body.appendChild(div);
//     };
//     return CreateDiv;
// })();

// var a = new CreateDiv('a1');
// var b = new CreateDiv('a2');
// console.log(a === b);

// 在浏览器打开

// 普通单例模式
var Person = function(name) {
    this.name = name;
}

Person.instance = null;
Person.prototype.getName = function(name) {
    console.log(name);
}

Person.getInstance = function(name) {
    if (!this.instance) {
        this.instance = new Person(name);
    }
    return this.instance;
}
var a = Person.getInstance('person1');
var b = Person.getInstance('person2');

console.log(a === b);