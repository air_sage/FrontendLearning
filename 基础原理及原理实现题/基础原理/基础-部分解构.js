let person = {
    name: 'Airsage',
    age: 27
};

let personName, personBar, personAge;
try {
    ({ name: personName, foo: { bar: personBar }, age: personAge } = person);
} catch (e) { console.log(e); };
console.log(personName, personBar, personAge);