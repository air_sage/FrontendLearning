// 注意两种写法的区别

const map = new Map();
map[1] = 1;
map[2] = 2;
console.log(map); // Map(0) { '1': 1, '2': 2 }

const map1 = new Map();
map1.set(1, 1);
map1.set(2, 2);
console.log(map1); // Map(2) { 1 => 1, 2 => 2 }