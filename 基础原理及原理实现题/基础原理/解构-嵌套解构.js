let person = {
    name: 'airsage',
    age: 23,
    job: {
        title: 'hello wolrd'
    }
};

let personCopy = {};

({
    name: personCopy.name,
    age: personCopy.age,
    job: personCopy.job,
} = person);

console.log(personCopy);