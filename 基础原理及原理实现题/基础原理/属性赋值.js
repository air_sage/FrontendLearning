let name1 = "airsage";
let name2 = new String("airsage");

// name1.age = 27;
// name2.age = 26;
// console.log(name1.age);
// console.log(name2.age);
// console.log(typeof name1);
// console.log(typeof name2);

let num = new Number(2);
let num2 = 2;
console.log(typeof num.toString()); // toString()返回字符串
console.log(typeof num);
console.log(num.valueOf() === parseInt(num.toString()))

console.log(num instanceof Number);
console.log(num2 instanceof Number);