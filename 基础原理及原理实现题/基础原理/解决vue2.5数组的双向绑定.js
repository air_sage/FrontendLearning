// 由于Object.defineProperty只能作用于对象上，对数组不起作用
// 所以我们需要手动更改数组原型，并重写方法
const oldArrayProperty = Array.prototype;
const newArrayProperty = Object.create(oldArrayProperty);
['pop', 'push', 'shift', 'unshift', 'splice'].forEach(method => {
    newArrayProperty[method] = function() {
        renderView();
        oldArrayProperty[method].call(this, ...arguments);
    }
});

// 在observer函数中加入数组的判断，如果传入的是数组，则改变数组的原型对象为我们修改过后的原型。
if (Array.isArray(target)) {
    target.__proto__ = newArrayProperty;
}

// 现在我们可以看出Object.defineProperty的一些问题

// 1.递归遍历所有的对象的属性，这样如果我们数据层级比较深的话，是一件很耗费性能的事情
// 2.只能应用在对象上，不能用于数组
// 3.只能够监听定义时的属性，不能监听新加的属性，这也就是为什么在vue中要使用Vue.set的原因，删除也是同理