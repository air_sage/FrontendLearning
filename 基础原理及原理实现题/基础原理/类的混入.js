class Father {};
let A = (superClass) => class extends superClass {
    methodA() {
        console.log('A');
    }
}

let B = (superClass) => class extends superClass {
    methodB() {
        console.log('B');
    }
}

let C = (superClass) => class extends superClass {
    methodC() {
        console.log('C');
    }
}

// 嵌套写法
class Child extends A(B(C(Father))) {}

// 扁平化写法
function mix(BaseClass, ...Mixins) {
    return Mixins.reduce((acc, cur) => cur(acc), BaseClass);
}
class Child2 extends mix(Father, A, B, C) {};

let child = new Child();
child.methodA();
child.methodB();
child.methodC();

let child2 = new Child2();
child2.methodA();
child2.methodB();
child2.methodC();