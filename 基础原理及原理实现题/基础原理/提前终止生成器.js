function* generatorFn() {
    for (const x of[1, 2, 3]) {
        yield x;
    }
}

const g = generatorFn();

for (const x of g) {
    if (x > 1) {
        g.return(6);
    }
    console.log(x);
}
console.log(g.next().done);
// 一旦done为true，就无法再开启，所以下面的代码不会执行
for (const x of g) {
    if (x > 1) {
        g.return(6);
    }
    console.log(x);
}