var book = {
    year_: 2017,
    verson: 1
};

Object.defineProperty(book, "year", {
    get() {
        // book.year
        return this.year_;
    },
    set(newValue) {
        if (newValue > 2017) {
            this.year_ = newValue;
            this.verson += this.year_ - 2017;
        }
    }
})

book.year = 2018;
console.log(book.verson);
console.log(book.year);