let person = {};
Object.defineProperty(person, "name", {
    writable: false,
    value: "AirSage"
});

console.log(person.name);

person.name = "ZengShengzhao";
console.log(person.name);