const target = {
    foo: 'bar'
}

const handle = {
    get(trapTarget, property, receiver) {
        return trapTarget[property];
    }
}

const proxy = new Proxy(target, handle);

console.log(proxy.foo);