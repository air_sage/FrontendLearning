function spawn(genF) {
    return new Promise((resolve, reject) => {
        const gen = genF();

        function step(nextF) {
            let next;
            try {
                next = nextF;
            } catch (e) {
                return reject(e);
            }
            if (next.value) {
                return resolve(next.value);
            }
            Promise.resolve(next.value).then(v => {
                step(() => gen.next(v));
            }, e => {
                step(() => gen.throw(e));
            })
        }
        step(() => gen.next(undefined));
    })
}

async function fn(args) {

}

// 等价于

function fn(args) {
    return spawn(function*() {

    });
}