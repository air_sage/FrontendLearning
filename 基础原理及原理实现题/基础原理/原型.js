function Person() {}
let person = new Person();

console.log(Person.prototype); // {}
console.log(Person.prototype.__proto__); // [Object: null prototype] {}
console.log(Person.prototype.constructor); // [Function: Person]
console.log(Person.prototype.prototype); // undefined
console.log(Person.__proto__); // {}
// console.log(Object); // [Function: Object]
// console.log(Object.prototype); // [Object: null prototype] {}
// console.log(Object.prototype.constructor); // [Function: Object]
console.log(person.__proto__); // {}
console.log(person.prototype); // undefined

console.log(Person.prototype.constructor === Person); // true
console.log(person.__proto__ === Person.prototype); //true
console.log(Person.prototype.__proto__ === Object.prototype); // true
console.log(person.__proto__ === Person.__proto__); //false