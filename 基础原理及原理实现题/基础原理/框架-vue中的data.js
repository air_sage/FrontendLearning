// 创建一个简单的构建函数
var MyComponent = function() {
        // ... code here
        this.data = this.data();

        // this.data.name = this.data().name;
        // this.data.age = this.data().age;
    }
    // 原型链对象上设置data数据，为里为Object
MyComponent.prototype.data = function() {
    return {
        name: 'abc',
        age: 20,
    }

};

function dataFull() {
    var obj = {
        name: 'ab',
        age: 23,
    };
    return obj
}
let a = dataFull();
let b = dataFull();
console.log(a === b);

// 创建两个实例:小明，小红
var xiaoming = new MyComponent()
var xiaohong = new MyComponent()
    // 默认状态下小明和小红的年龄一样
console.log(xiaoming.data === xiaohong.data) // false;
console.log(xiaoming.data.age === xiaohong.data.age) // true
    // 改变一下小明的年龄
xiaoming.data.age = 30;
// 打印下小红的年龄，发现因为必变了小明的年龄，结果造成小红的年龄也变了
console.log(xiaoming.data.age) //30
console.log(xiaohong.data.age) // 20