const target = {
    foo: 'bar',
    baz: 'qux'
}
const handle = {
    get(trapTarget, property, receiver) {
        let decleartion = '';
        if (property === 'foo') decleartion = '!!!';
        return Reflect.get(...arguments) + decleartion;
    }
}

const proxy = new Proxy(target, handle);
console.log(proxy.foo);
console.log(target.foo);
console.log(proxy.baz);
console.log(target.baz);