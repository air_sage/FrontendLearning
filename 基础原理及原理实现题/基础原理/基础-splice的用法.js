let arr = [0, 1, 2, 3, 4];

// splice方法返回的是一个新数组，如果原数组有元素被删除或替换，都会出现在这个数组中
// 如果是solice添加元素，返回空数组

// splice不加参数，原数组没有任何变化，返回空数组
console.log(arr.splice());

// splice加一个参数，表示截取的长度
// console.log(arr.splice(2));

// 删除某一位置上的元素
// console.log(arr.splice(3, 1));

// 删除固定长度的一些元素
// console.log(arr.splice(1, 3));

// 替换某一位置上的元素
// console.log(arr.splice(2, 1, 7));

// 多个元素被一个元素替换
// console.log(arr.splice(2, 2, 7));

// 在某个位置上插入一个元素
// console.log(arr.splice(2, 0, 1));

// 在某个位置添加多个元素
console.log(arr.splice(2, 1, 9, 9, 9));

console.log(arr);