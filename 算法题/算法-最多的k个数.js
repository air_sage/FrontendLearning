function topKFrequent(nums, k) {
    // write code here
    const map = new Map();
    nums.forEach(item => {
        console.log(item);
        if (map.has(item)) {
            map.set(item, map.get(item) + 1);
        } else {
            map.set(item, 1);
        }
    })
    console.log(map);
    let res = [];
    for (let num of map.keys()) {
        console.log(num);
        res.push(num);
    }
    res.sort((a, b) => map.get(b) - map.get(a));
    res.splice(k);
    return res;
}

let result = topKFrequent([1, 1, 1, 2, 2, 3], 2);
console.log(result);