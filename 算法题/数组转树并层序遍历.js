var readline = require('readline');

let rows = [];
let k = -1;
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line => {
    if (k == -1) {
        k = parseInt(line.trim());
    } else {
        rows.push(line);
        if (k == rows.length) {
            // ********核心算法区************
            // 解析rows为对象数组
            let arr = parse(rows);
            // 生成根节点树
            let tree = createTree(arr);
            // 层序遍历
            levelOrder(tree);
            // *****************************
            rl.close();
        }
    }
})

var parse = function(data) {
    let arr = [];
    data.forEach(li => {
        // 把字符串转换为数组
        let numArr = li.split(' ').map(item => parseInt(item));
        // 创建对象
        let obj = {};
        obj.node = numArr[0];
        obj.children = numArr.slice(1, numArr.length);
        // 把对象放入数组
        arr.push(obj);
    })
    console.log("arr:", arr);
    return arr;
}

var createTree = function(arr) {
    // 创建Map
    const mp = new Map();
    // 遍历数组，把数组的每个对象存入Map
    arr.forEach(obj => {
            // 先默认所有节点都没有父节点，如果节点有孩子就写入孩子，没有孩子创建空数组
            mp[obj.node] = { node: obj.node, parent: 0, children: obj.children || [] };
        })
        // 对数组中对象的每个孩子，进行替换
    arr.forEach(obj => {
        for (let i = 0; i < obj.children.length; i++) {
            // 如果孩子曾经创建过，先更改父节点，再替换
            if (mp[obj.children[i]]) {
                mp[obj.children[i]].parent = obj.node;
                obj.children[i] = mp[obj.children[i]];
            }
        }
    })
    console.log(mp);

    // 找根节点,parent == 0的对象为根节点
    let rootKey;
    for (key in mp) {
        if (mp[key].parent == 0)
            rootKey = key;
    }
    console.log("tree:", mp[rootKey]);
    return mp[rootKey];
}


var levelOrder = function(root) {
    const res = [];
    if (root == null) console.log('');
    const queue = [];
    queue.push(root);
    while (queue.length) {
        // const level = [];
        // const len = queue.length;
        // for (let i = 0; i < len; i++) {
        const el = queue.shift();
        if (typeof el == 'number') {
            res.push(el);
        } else {
            res.push(el.node);
            queue.push(...el.children);
        }
        // }
        // res.push(level);
    }
    console.log("result Array:", res);
    let str = res.join(' ');
    console.log("result:", str);
    return res;
}