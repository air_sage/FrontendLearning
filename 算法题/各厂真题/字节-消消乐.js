let str = 'adaadaccc';
// b立即删除 ，连续ac出现，删除ac
function solve(str) {
    const stack = [];
    for (let i = 0; i < str.length; i++) {
        if (str[i] !== 'b' && str[i] !== 'c') {
            stack.push(str[i]);
        } else if (str[i] === 'c') {
            stack[stack.length - 1] === 'a' ? stack.pop() : stack.push(str[i]);
        }
    }
    return stack.join('');
}

console.log(solve(str));