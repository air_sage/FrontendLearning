// 计算数组的小和

/**
 * the min sum of the array
 * @param arr int整型一维数组 the array
 * @return long长整型
 */
function minSum(arr) {
    // write code here
    function mergeSort(arr, start, end) {
        if (!Array.isArray(arr) || !arr.length || start >= end) {
            return 0;
        }
        let mid = Math.floor((start + end) / 2);
        return mergeSort(arr, start, mid) + mergeSort(arr, mid + 1, end) + merge(arr, start, mid, end);
    }

    function merge(arr, start, mid, end) {
        let left = start;
        let right = mid + 1;
        const res = [];
        let sum = 0;
        let newIndex = 0;
        while (left <= mid && right <= end) {
            if (arr[left] <= arr[right]) {
                sum += arr[left] * (end - right + 1);
                res[newIndex++] = arr[left++];
            } else if (arr[left] > arr[right]) {
                res[newIndex++] = arr[right++];
            }
        }
        while (left <= mid) {
            res[newIndex++] = arr[left++];
        }
        while (right <= end) {
            res[newIndex++] = arr[right++];
        }
        for (let i = 0; i < res.length; i++) {
            arr[start++] = res[i];
        }
        return sum;
    }
    if (!arr.length) {
        return 0;
    }
    let n = arr.length;
    return mergeSort(arr, 0, n - 1);
}
module.exports = {
    minSum: minSum
};

// 求最大数

/**
 * 最大数
 * @param nums int整型一维数组 
 * @return string字符串
 */
function solve(nums) {
    // write code here
    let arr = nums.map(item => String(item));
    arr.sort((a, b) => {
        let res1 = a + b;
        let res2 = b + a;
        return parseInt(res2) - parseInt(res1);
    })
    if (arr[0] === '0') {
        return '0';
    }
    return arr.join('');

}
module.exports = {
    solve: solve
};