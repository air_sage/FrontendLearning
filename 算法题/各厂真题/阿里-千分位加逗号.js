function fn(num) {
    let str = '';
    let s = "" + num;
    console.log(s);
    let n = 0;
    for (let i = s.length - 1; i >= 0; i--) {
        str += s[i];
        n++;
        if (n % 3 === 0) {
            str += ',';
        }
    }
    console.log(str);
    str = str.split('').reverse().join('');
    if (str[0] == ',') {
        str = str.slice(1);
    }
    return str;
    // while (num > 0) {
    //     let temp = num % 10;
    //     str += temp;
    //     console.log(num);
    //     n++;
    //     if ((n % 3) == 0) {
    //         str += ',';
    //     }
    //     num = num / 10;
    // }
    // let i = 0;
    // let j = str.length - 1;
    // while (i < j) {
    //     [str[i], str[j]] = [str[j], str[i]];
    // }
}

let result = fn(658614684133);
console.log(result);


function fn1(str) {
    let len = str.length;
    let count = 0;
    let s = '';
    for (let i = len - 1; i >= 0; i--) {
        s = s + str[i];
        if (++count === 3) {
            s = s + ',';
            count = 0;
        }
    }
    let res = '';
    for (let i = s.length - 1; i >= 0; i--) {
        res = res + s[i];
    }
    if (res[0] === ',') {
        return res.slice(1);
    }
    return res;
}

console.log(fn1("31351043643"));

function addComma(num) {
    return String(num).replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}
console.log(addComma(1456978315));