var readline = require('readline');

let rows = [];

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line => {
    rows.push(line);
    if (rows[rows.length - 1] === '0') {
        rows.pop();
        rows.forEach(li => {
            // 此处有个
            console.log(calculate(li));

        })
        rl.close();
    }
})

var calculate = function(s) {
    let sign = '+',
        n = 0,
        c, stack = [];
    for (let i = 0; i <= s.length; i++) {
        c = s.charAt(i);
        if (c === ' ') continue;
        if (c <= '9' && c >= '0') {
            n = n * 10 + parseInt(c);
            continue;
        }
        if (sign === '+') {
            stack.push(n);
        } else if (sign === '-') {
            stack.push(-n);
        } else if (sign === '*') {
            stack.push(stack.pop() * n);
        } else if (sign === '/') {
            stack.push(stack.pop() / n);
        }
        sign = c;
        n = 0;
    }
    return stack.reduce((acc, n) => acc + n, 0).toFixed(2);
}