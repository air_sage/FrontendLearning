function solve1(n) {
    const dp = [0,1,1];
    for(let i = 3; i <= n; i++) {
        dp[i] = dp[i-1] + dp[i-2];
    }
    return dp[n];
}


function solve2(str) {
    if(str.length % 2 !== 0) return false;
    const map = new Map();
    map.set(')','(');
    map.set(']','[');
    map.set('}','{');
    const stack = [];
    for(let i = 0; i < str.length; i++) {
        if(str[i] === ')' || str[i] === ']' || str[i] === '}') {
            if(stack[stack.length - 1] === map.get(str[i])) {
                stack.pop();
            } else {
                return false;
            }
        } else {
            stack.push(str[i]);
        }
    }
    return stack.length === 0;
}

// console.log(solve2('()[]{}'));

// console.log(solve1(8));


function solve3(a, b) {
    const map = new Map();
    for(let i = 0; i < a.length; i++) {
        if(map.has(a[i])) {
            map.set(a[i], map.get(a[i]) + 1);
        } else {
            map.set(a[i], 1);
        }
    }

    for(let j = 0; j < b.length; j++) {
        if(!map.has(b[j]) || map.get(b[j]) === 0) return false;
        else {
            map.set(b[j], map.get(b[j]) - 1);
        }
    }
    return true;
}

function solve4(a, b) {
    let index = 0;
    for(let i = 0; i < b.length; i++) {
        while(b[i] > a[index] && index < a.length) {
            index++;
        }
        if(b[i] === a[index]) {
            index++;
        } else {
            return false;
        }  
    }
    return true;
}


var a = [1, 1, 4, 7, 9];
var b = [1, 3, 9, 9];

console.log(solve4(a, b));
