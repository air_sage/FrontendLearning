const resp = {
    message: 'success',
    code: 0,
    data: [
        [{ title: 'nihao' }], {
            title: 'hello'
        }
    ]
};

function get(resp, ...args) {
    let arr = [...args];
    let arr1 = [];
    let res = [];
    for (let i = 0; i < arr.length; i++) {
        arr1.push(arr[i].split('.'));
    }
    console.log(arr1);
    for (let i = 0; i < arr1.length; i++) {

        let r;

        for (let j = 0; j < arr1[i].length; j++) {
            if (arr1[i][j].indexOf('[') !== -1 && arr1[i][j].indexOf(']') !== -1) {
                // 匹配变量名
                let exg = arr1[i][j].match(/^[a-zA-Z0-9_-]{1,}(?=\[(.+?)\])/g);
                // 匹配中括号
                let num = arr1[i][j].match(/\[(.+?)\]/g);
                // 数组n用来保存中括号里面的数字
                let n = [];

                // console.log(num);

                // 把中括号去掉，变成真正的数组
                if (num) {
                    for (let i = 0; i < num.length; i++) {
                        // console.log(num[i].substring(1, num[i].length - 1));
                        let s = num[i].substring(1, num[i].length - 1);
                        n.push(parseInt(s));
                    }
                }
                // console.log(n);
                // 如果变量名有匹配
                if (exg) {
                    // 如果变量r还没赋值，就是根属性
                    if (!r) r = resp[exg[0]];
                    // 如果是非根属性
                    else {
                        let temp = r[exg[0]];
                        r = temp;
                    }
                }
                // 对于每一个中括号里面的数字，进行遍历
                while (n.length) {
                    let temp = r[n.shift()];
                    r = temp;
                    // console.log(r);
                }
            } else {
                // 如果不含有中括号，直接使用
                // console.log(arr1[i][j]);
                if (!r) r = resp[arr1[i][0]];
                else {
                    // console.log(r);
                    let temp = r[arr1[i][j]];
                    // console.log(temp);
                    r = temp;
                }
            }

        }
        res.push(r);
        r = null;
    }
    return res;

}

let res = get(resp, 'message', 'data[0][0].title');

console.log(res);