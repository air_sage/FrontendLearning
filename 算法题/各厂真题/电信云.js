function solve(arr, n) {
    while (n--) {
        let temp = arr.pop();
        arr.unshift(temp);
    }
    return arr;
}

const arr = [1, 2, 3, 4, 5, 6, 7];
let n = 3;
let res = solve(arr, n);
console.log(res);