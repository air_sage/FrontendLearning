var readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

let rows = [];

rl.on("line", line => {
    rows.push(line.split(' ').map(item => parseInt(item)));
    if (rows.length == 2) {

        let num = rows[0][0];
        let magic = rows[0][1];
        let numArr = rows[1];
        makeCar(num, magic, numArr);
        rl.close();
    }
})

var makeCar = function(n, magicNum, arr) {
    arr.sort((a, b) => a - b);
    console.log(arr);
    for (let i = 0; i < n - 1; i++) {
        if (arr[i] < arr[i + 1]) {
            magicNum = magicNum - i;
            if (magicNum <= 0) console.log(arr[i]);
        }
    }
    if (magicNum > 0) console.log(arr[n - 1]);
}