function addCommaEachThousand(numStr) {
    const arr = numStr.split('.').map(item => parseInt(item));
    let s = String(arr[0]);
    let res = '';
    let n = 0;
    for (let i = s.length - 1; i >= 0; i--) {
        res += s[i];
        n++;
        if (n % 3 == 0) {
            res += ',';
        }
    }
    res = res.split('').reverse().join('');
    if (res[0] == ',') {
        res = res.slice(1);
    }
    if (arr.length == 2) {
        res += '.' + arr[1];
    }
    return res;
}

const numString = '111222333.123';
let res1 = addCommaEachThousand(numString);
console.log(res1);