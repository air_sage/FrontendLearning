const readline = require('readline');
var flag = true;
var cl = readline.createInterface(process.stdin, process.stdout);

function ifWin(x, y, flag) {
    map[x][y] = flag;
    let lenX = map.length;
    let lenY = map[0].length;

    // 水平方向
    let tempX = x;
    let count1 = 0;
    while (true) {
        if (tempX >= lenX || tempX < 0) {
            break;
        }
        if (map[tempX][y] === flag) {
            count1++;
            tempX++;
        } else {
            break;
        }
    }
    tempX = x - 1;
    while (true) {
        if (tempX >= lenX || tempX < 0) {
            break;
        }

        // console.log(map[tempX][y] === flag);
        if (map[tempX][y] === flag) {
            count1++;
            tempX--;
        } else {
            break;
        }
    }

    // 垂直方向
    let tempY = y;
    let count2 = 0;
    while (!(tempY >= lenY || tempY < 0) && map[x][tempY] === flag) {
        count2++;
        tempY++;
    }
    tempY = y - 1;
    while (!(tempY >= lenY || tempY < 0) && map[x][tempY] === flag) {
        count2++;
        tempY--;
    }

    // 正斜角方向
    let tempX1 = x, tempY1 = y;
    let count3 = 0;
    while (!(tempX1 >= lenX || tempX1 < 0 || tempY1 > lenY || tempY1 < 0) && map[tempX1][tempY1] === flag) {
        count3++;
        tempX1++;
        tempY1++;
    }
    tempX1 = x - 1, tempY1 = y - 1;
    while (!(tempX1 >= lenX || tempX1 < 0 || tempY1 >= lenY || tempY1 < 0) && map[tempX1][tempY1] === flag) {
        count3++;
        tempX1--;
        tempY1--;
    }

    // 反斜角方向
    let tempX2 = x, tempY2 = y;
    let count4 = 0;
    while (!(tempX2 >= lenX || tempX2 < 0 || tempY2 >= lenY || tempY2 < 0) && map[tempX2][tempY2] === flag) {
        count4++;
        tempX2++;
        tempY2--;
    }
    tempX2 = x - 1, tempY2 = y + 1;
    while (!(tempX2 >= lenX || tempX2 < 0 || tempY2 >= lenY || tempY2 < 0) && map[tempX2][tempY2] === flag) {
        count4++;
        tempX2--;
        tempY2++;
    }

    if (count1 >= 5 || count2 >= 5 || count3 >= 5 || count4 >= 5) {
        console.log(`${flag ? '白方' : '黑方'} 胜利!`);
        return true;
    } else {
        flag = !flag;
        return false;
    }

}

// const map = [[0, 0, 0, -1, 0, -1, 1, 0],
//             [0, 0, 0, 0, -1, 0, 0, 0],
//             [0, 0, 1, 0, 1, -1, 1, 0],
//             [0, 0, 0, 1, 0, -1, 0, 0],
//             [0, 0, 1, 0, 0, -1, 0, -1],
//             [0, 0, 0, 0, 0, 0, 0, 0],
//             [0, 0, 0, 0, 0, 0, 0, 0],
//             [0, 0, 0, 0, 0, 0, 0, 0]]

// const map = [
//     [0, 0, 0, 0, 0, 0, 0, 0],
//     [0, 0, 0, 0, 0, 0, 0, 0],
//     [0, 0, 0, 0, 0, 0, 0, 0],
//     [0, 0, 0, 0, 0, 0, 0, 0],
//     [0, 0, 0, 0, 0, 0, 0, 0],
//     [0, 0, 0, 0, 0, 0, 0, 0],
//     [0, 0, 0, 0, 0, 0, 0, 0],
//     [0, 0, 0, 0, 0, 0, 0, 0]
// ];

const map = [
    ['-', '-', '-', '-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-', '-', '-', '-'],
    ['-', '-', '-', '-', '-', '-', '-', '-']
]


var question = function (q) {
    return new Promise((resolve, reject) => {
        cl.question(q, str => {
            let arr = str.split(' ').map(item => parseInt(item));
            let person = flag ? 'O' : 'X';
            if(arr[0] >= map.length || arr[0] < 0 || arr[1] >= map.length || arr[1] < 0) {
                console.log('超过棋盘范围，请下在正确的位置上!')
                resolve({ isWin: false, change: false });
            } else if (map[arr[0]][arr[1]] !== '-') {
                console.log('该位置已经下过棋子,请下在其他位置上!');
                resolve({ isWin: false, change: false });
            } else {
                let res = ifWin(arr[0], arr[1], person);
                resolve({ isWin: res, change: true });
            }
            console.log('\t' + [0, 1, 2, 3, 4, 5, 6, 7].join('\t'));
            for (let i = 0; i < map.length; i++) {
                console.log(i + '\t' + map[i].join('\t'));
            }
        })
    });
};

(async function main() {
    let isWin = false;
    while (!isWin) {
        result = await question(`${flag === true ? '白方' : '黑方'}请下棋(格式: 横坐标 纵坐标)`);
        isWin = result.isWin;
        flag = result.change ? !flag : flag;
    }
    cl.close();
})()