// function sortStr(str) {
//     const map = new Map();
//     for (let i = 0; i < str.length; i++) {
//         console.log(str[i]);
//         if (map.has(str[i])) {
//             map.set(str[i], map.get(str[i]) + 1);
//         } else {
//             map.set(str[i], 1);
//         }
//     }
//     const arr = [];
//     for (let i = 1; i <= 9; i++) {
//         let temp = String(i);
//         if (map.has(temp)) {
//             arr.push(temp);
//         }
//     }
//     let char = 'abcdefghijklmnopqrstuvwxyz';
//     for (c of char) {
//         if (map.has(c)) {
//             arr.push(c);
//         }
//     }
//     let res = arr.map(item => {
//         return item + map.get(item);
//     })
//     return res.join('');
// }

// let res = sortStr('indepe11nd2e2nt');
// console.log(res);


function solve(str) {
    const stack = [];
    for (let i = 0; i < str.length; i++) {
        if (str[i] === '(' || str[i] === '[' || str[i] === '{') {
            stack.push(str[i]);
        } else {
            if (str[i] === ')' && stack[stack.length - 1] === '(' || str[i] === ']' && stack[stack.length - 1] === '[' || str[i] === '}' && stack[stack.length - 1] === '{') {
                stack.pop();
            } else {
                stack.push(str[i]);
            }
        }
    }
    return stack.length === 0;
}

console.log(solve('()'));
console.log(solve('()[]{}'));
console.log(solve('(]'));
console.log(solve('([)]'));
console.log(solve('{[]}'));
console.log(solve(')))'));
console.log(solve('((('));