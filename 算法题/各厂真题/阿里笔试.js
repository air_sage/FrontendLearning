/*
 问题：1: 为数字（数字包含整数，小数）插入千分位符号。
 
 	如111222333.123，千分位分割后是”111，222，333.123“。
*/
function addCommaEachThousand(numStr) {
    const arr = numStr.split('.').map(item => parseInt(item));
    let s = String(arr[0]);
    let res = '';
    let n = 0;
    for (let i = s.length - 1; i >= 0; i--) {
        res += s[i];
        n++;
        if (n % 3 == 0) {
            res += ',';
        }
    }
    res = res.split('').reverse().join('');
    if (res[0] == ',') {
        res = res.slice(1);
    }
    if (arr.length == 2) {
        res += '.' + arr[1];
    }
    return res;
}

const numString = '111222333.123';
let res1 = addCommaEachThousand(numString);
console.log(res1);



/*
 问题：2: 有两个二叉树，判断其中一个是不是另外一个子树。
 
   二叉树节点的结构如下：
   interface TreeNode {
      value: 'xxx',
      left?: TreeNode,
      right?: TreeNode,
   }
*/

function TreeNode(value, left, right) {
    this.value = value === undefined ? 0 : value;
    this.left = left === undefined ? null : left;
    this.right = right === undefined ? null : right;
}

function buildTree(arr, index) {
    if (index > arr.length) return null;
    let value = arr[index - 1];
    if (value == null) return null;
    const root = new TreeNode(value);
    root.left = buildTree(arr, index * 2);
    root.right = buildTree(arr, index * 2 + 1);
    return root;
}

function isChildTreeOf(c, f) {
    if (!f) return false;
    if (isSameTree(c, f)) return true;
    return isChildTreeOf(c, f.left) || isChildTreeOf(c, f.right);
}

function isSameTree(c, f) {
    if (!c && !f) return true;
    if (c && f && c.value === f.value && isSameTree(c.left, f.left) && isSameTree(c.right, f.right)) {
        return true;
    }
    return false;
}

const A = [3, 4, 5];
const B = [1, 2, 3, null, null, 4, 5];
const treeA = buildTree(A, 1);
const treeB = buildTree(B, 1);
let res2 = isChildTreeOf(treeA, treeB);
console.log(res2);


/*
 问题3：一个字符串，给出所有可能的排列，假设没有重复
 
 如：let str = 'bcdefgcba';
*/

function findAllString(str) {
    let n = str.length;
    let res = [];
    const arr = str.split('');

    function dfs(x) {
        if (x === n - 1) {
            res.push(arr.join(''));
            return;
        }
        const set = new Set();
        for (let i = x; i < n; i++) {
            if (!set.has(arr[i])) {
                set.add(arr[i]);
                [arr[i], arr[x]] = [arr[x], arr[i]];
                dfs(x + 1);
                [arr[i], arr[x]] = [arr[x], arr[i]];
            }
        }
    }
    dfs(0);
    return res;
}

const str = 'bcdefgcba';
const res3 = findAllString(str);
console.log(res3);


function isSubTree(c, f) {
    if (!c && !f) return true;
    if (!c || !f) return false;
    return isSameTree(c, f) || isSubTree(c, f.left) || isSubTree(c, f.right);
}

function isSameTree(c, f) {
    if (!c && !f) return true;
    if (!c || !f) return false;
    return c.value === f.value && isSameTree(c.left, f.left) && isSameTree(c.right, f.right);
}