function ReverseList(head) {
    let newHead = null;
    while (head) {
        let temp = head.next;
        head.next = newHead;
        newHead = head;
        head = temp;
    }
    return newHead;

}