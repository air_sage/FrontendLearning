// let arr = [];

// let arr1 = [];

// for (let i = arr.length - 1; i >= 0; i--) {
//     if (arr[i].parentId !== 0) {
//         let j = arr[i].parentId - 1;
//         if (!arr[j].children) arr[j].children = [];
//         arr[j].children.push(arr[i]);
//         arr[j].children.sort((a, b) => {
//             a.id < b.id;
//         })
//     } else {
//         arr1.push(arr[i]);
//         arr1.sort((a, b) => {
//             a.id < b.id;
//         })
//     }
// }
// console.log(arr1);

arr2 = [{
        id: 1,
        name: '部门A',
        parentId: 0,
        children: [{
                id: 3,
                name: '部门C',
                parentId: 1,
                children: [
                    { id: 6, name: '部门F', parentId: 3 }
                ]
            },
            {
                id: 4,
                name: '部门D',
                parentId: 1,
                children: [
                    { id: 8, name: '部门H', parentId: 4 }
                ]
            }
        ]

    },
    {
        id: 2,
        name: '部门B',
        parentId: 0,
        children: [
            { id: 5, name: '部门E', parentId: 2 },
            { id: 7, name: '部门G', parentId: 2 }
        ]
    }
]


// 1.深度优先
// let res = [];

// function traverseTree(arr) {
//     for (let i = 0; i < arr.length; i++) {
//         let temp = {
//             id: arr[i].id,
//             name: arr[i].name,
//             parentId: arr[i].parentId
//         }
//         res.push(temp);
//         if (arr[i].children) {
//             traverseTree(arr[i].children);
//         }
//     }
// }

// traverseTree(arr2);
// let compare = function(a, b) {
//     let idA = a.id;
//     let idB = b.id;
//     if (idA < idB) return -1;
//     else if (idA > idB) return 1;
//     else 0;
// }

// res.sort(compare);
// console.log(res);

// 2.广度优先
let queue = [];
let res = [];
for (let i = 0; i < arr2.length; i++) {
    res.push({
        id: arr2[i].id,
        name: arr2[i].name,
        parentId: arr2[i].parentId
    });
    if (arr2[i].children) queue.push(arr2[i].children);
}
while (queue.length !== 0) {
    let temp = queue.shift();
    for (let j = 0; j < temp.length; j++) {
        res.push({
            id: temp[j].id,
            name: temp[j].name,
            parentId: temp[j].parentId
        });
        if (temp[j].children) queue.push(temp[j].children);
    }
}
console.log(res);