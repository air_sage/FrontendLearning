function shuffle(arr) {
    let temp = [...arr];
    return function(n) {
        let res = [];
        for (let i = 0; i < n; i++) {
            if (temp.length == 0) {
                temp = [...arr];
            }
            let randomIndex = Math.floor(Math.random() * temp.length);
            [temp[0], temp[randomIndex]] = [temp[randomIndex], temp[0]];
            res.push(temp.shift());
        }
        return res;
    }
}

var random = shuffle([0, 1, 2, 3, 4, 5, 6]);
console.log(random(1));
console.log(random(2));
console.log(random(1));
console.log(random(4));
console.log(random(6));
console.log(random(7));