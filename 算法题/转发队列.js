let len = 5;
let arr = readline().split(' ').map(item => {
    return item.split(',').map(it => parseInt(it));
});
// let arr = [
//     [50, 50],
//     [20, 20],
//     [40, 10],
//     [30, 5],
//     [10, 5]
// ];

let cacheArr = [];
let i = 0;
let input = 20;
let res1 = input;

while (i < len) {
    let send = arr[i][0];
    let cache = arr[i][1];
    if (res <= send) {
        res = send;
    } else if (res > send) {
        let temp = res - send;
        res = send;
        temp <= cache ? cacheArr.push(temp) : cacheArr.push(cache);
    }
    i++;
}

i = 0;
let res2;
while (i < len) {
    let send = arr[i][0];
    // let cache = arr[i][1];
    let cach = cacheArr[i];
    if (i == 0) {
        res2 = cach <= send ? cach : send;
    } else {
        let temp = res2 + cach;
        res2 = temp <= send ? temp : send;
    }
    i++;
}
return res1 + res2;