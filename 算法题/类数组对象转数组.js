var arrayLike = {};

arrayLike[0] = "test 0";
arrayLike[1] = "test 1";
arrayLike[2] = "test 2";
arrayLike[3] = "test 3";

//关键点
arrayLike.length = 4; //为对象设置length属性
arrayLike.splice = [].splice; //同时设置splice属性为一个函数

console.log(arrayLike);
// console.log($.type(arrayLike));
console.log(arrayLike[2]);

// 类数组转数组
// 法一 Array.prototye.slice.call(arguments);
function arrayLikeToArray() {
    return Array.prototype.slice.call(arguments);
}
// let list1 = arrayLikeToArray(arrayLike);
// console.log(list1);

// 法二
const arrayLike2 = {
    length: 5
};

let arr1 = Array.from(arrayLike);
let arr2 = Array.from(arrayLike2);
console.log(arr1);
console.log(arr2);

// 兼容一二法
const toArray = (() => Array.from ? Array.from : obj => [].slice.call(obj))();

// 法3 Object.values()
// 与Array.from不同的是Object.values不需要length属性，返回一个对象所有可枚举属性值
const obj1 = { 100: 'a', 2: 'b', 7: 'c' };
let objArr = Object.values(obj1);
console.log(objArr);

let indexArr = Object.keys(obj1);
console.log(indexArr);

let entriesArr = Object.entries(obj1);
console.log(entriesArr);

// 法4 [...xxx] 扩展运算符
function list2() {
    return [...arguments];
}

let arr3 = list2(arrayLike);
console.log(arr3);