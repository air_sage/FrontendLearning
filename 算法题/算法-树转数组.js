const tree = [
    { level: 1, children: [{ level: 2, children: [{ level: 3 }] }] },
    { level: 1 }
]
const res = [];

function treeToArr(tree) {
    if (tree instanceof Array && tree !== []) {
        for (let i = 0; i < tree.length; i++) {
            let temp = tree[i];
            res.push({ level: temp.level });
            treeToArr(temp.children);
        }
    }
}

treeToArr(tree);
console.log(res);

function treeToArr2() {
    const queue = [];
    const out = [];
    queue = queue.concat(tree);
}