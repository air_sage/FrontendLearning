function arrUnique(arr) {
    const obj = {};
    return arr.filter(item => {
        return obj.hasOwnProperty(typeof item + JSON.stringify(item)) ? false : obj[typeof item + JSON.stringify(item)] = true;
    })
}

function findSame(a, b) {
    const uniqueA = arrUnique(a);
    const uniqueB = arrUnique(b);
    const res = [];
    uniqueA.forEach(itemA => {
        uniqueB.forEach(itemB => {
            console.log(itemB);
            if (JSON.stringify(itemA) === JSON.stringify(itemB)) {
                res.push(itemA);
            }
        })

    })
    return res;
}

var a = [123, "webank", [1, 2, 3], "123", { a: 1 }, "tencent", 123, [1, 2, 3], { a: 1 }];
var b = [123, "123455", { a: 2 },
    [1, 2, 3], "hello", "webank"
];

let res = findSame(a, b);
console.log(res);