function numWay(n) {
    let a = 1;
    let b = 1;
    let sum;
    for (let i = 0; i < n; i++) {
        sum = (a + b);
        a = b;
        b = sum;
    }
    return a;
}

function numWay1(n) {
    if (n < 2) {
        return 1;
    }
    let a = 1,
        b = 1,
        sum;
    for (let i = 2; i <= n; i++) {
        sum = (a + b);
        a = b;
        b = sum;
    }
    return b;
}

console.log(numWay1(100));

function solve(n) {
    if (n < 2) {
        return 1;
    }
    let a = 1;
    let b = 1;
    let sum;
    for (let i = 2; i <= n; i++) {
        sum = (a + b);
        a = b;
        b = sum;
    }
    return b;
}