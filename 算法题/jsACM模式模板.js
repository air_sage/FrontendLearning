var readline = require('readline');

let rows = [];
let k = -1;
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.on('line', line => {
    if (k == -1) {
        k = parseInt(line.trim());
    } else {
        rows.push(line);
        if (k == rows.length) {



            rows.forEach(li => {
                let numArr = li.split(' ').map(item => parseInt(item));
                let sum = 0;
                numArr.forEach((it) => {
                    sum += it;
                })
                console.log(sum);

            })
            rl.close();
        }
    }
})