const a = [1, [2, [3, [4, null]]]];
const b = [];

function reverseArr(arr) {
    const temp = [];
    while (arr != null) {
        temp.push(arr[0]);
        arr = arr[1];
    }
    const res = [];
    let p = res;
    while (temp.length !== 0) {
        p[0] = temp.pop();
        if (temp.length === 0) {
            p[1] = null;
        } else {
            p[1] = [];
            p = p[1];
        }
    }
    return res;
}

let res = reverseArr(a);
// console.log(res[1][1][1]);
console.log(res.valueOf());