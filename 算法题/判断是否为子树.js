function TreeNode(value, left, right) {
    this.value = value === undefined ? 0 : value;
    this.left = left === undefined ? null : left;
    this.right = right === undefined ? null : right;
}

function buildTree(arr, index) {
    if (index > arr.length) return null;
    let value = arr[index - 1];
    if (value == null) return null;
    const root = new TreeNode(value);
    root.left = buildTree(arr, index * 2);
    root.right = buildTree(arr, index * 2 + 1);
    return root;
}

function isChildTreeOf(c, f) {
    if (!f) return false;
    if (isSameTree(c, f)) return true;
    return isChildTreeOf(c, f.left) || isChildTreeOf(c, f.right);
}

function isSameTree(c, f) {
    if (!c && !f) return true;
    if (c && f && c.value === f.value && isSameTree(c.left, f.left) && isSameTree(c.right, f.right)) {
        return true;
    }
    return false;
}

const A = [3, 4, 5];
const B = [1, 2, 3, null, null, 4, 5];

const treeA = buildTree(A, 1);
const treeB = buildTree(B, 1);
console.log(treeB);
let res2 = isChildTreeOf(treeA, treeB);
console.log(res2);