let root = {
    value: 1,
    left: {
        value: 2,
        left: {
            value: 4,
            left:{
                value: 8
            }
        },
        right: {
            value: 5
        }
    },right: {
        value: 3
    }
}


function pathAdd(root) {

    let resPath = [];
    let res = 0;
    backtracking(root, []);
    console.log(resPath);
    resPath.forEach(item => {
        res += parseInt(item);
    })
    return res;
    function backtracking(node, path) {
        if(node.value) {
            path.push(node.value);
            if(!node.left && !node.right) resPath.push(path.join(''));
            if(node.left) backtracking(node.left, path);
            if(node.right) backtracking(node.right, path);
            path.pop();
        }

        // if(node.value && !node.left && !node.right) {
        //     path.push(node.value);
        //     resPath.push(path.join(''));
        //     path.pop();
        // } else if(node.value) {
        //     path.push(node.value);
        //     if(node.left) {
        //         backtracking(node.left, path);
        //     }
        //     if(node.right) {
        //         backtracking(node.right, path);
        //     }
        //     path.pop();
        // }
    }
}

let res = pathAdd(root);
console.log(res);