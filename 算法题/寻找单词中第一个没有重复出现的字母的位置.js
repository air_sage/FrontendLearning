// 寻找单词中第一个没有重复出现的字母的位置
// google -> 字母l -> 4
// dad -> 字母 a -> 1

const map = new Map();

var str = 'goodlecg';
var res = str.length;
for (let i = 0; i < str.length; i++) {
    if (map.has(str[i])) {
        map.set(str[i], map.get(str[i] + 1));
    } else {
        map.set(str[i], 1);
    }
}

for (key of map.keys()) {
    if (map.get(key) == 1) {
        res = Math.min(str.indexOf(key), res);
    }
}

if (res === str.length) {
    console.log(-1);
} else {
    console.log(res);
}