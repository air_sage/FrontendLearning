function findAllString(str) {
    let n = str.length;
    let res = [];
    const arr = str.split('');

    function dfs(x) {
        if (x === n - 1) {
            res.push(arr.join(''));
            return;
        }
        const set = new Set();
        for (let i = x; i < n; i++) {
            if (!set.has(arr[i])) {
                set.add(arr[i]);
                [arr[i], arr[x]] = [arr[x], arr[i]];
                dfs(x + 1);
                [arr[i], arr[x]] = [arr[x], arr[i]];
            }
        }
    }
    dfs(0);
    return res;
}

const str = 'bcdefgcba';
const res3 = findAllString(str);
console.log(res3);