// let line;
// while(line = readline()) {
//     var n = Number(line);
//     const mat = new Array();
//     for(let i = 0; i < n; i++) {
//         mat[i] = new Array(n);
//     }
//     let bottom = n - 1;
//     let top = 2;
//     let num = 1;
//     let nsquare = n * n;
//     let x = 0, y = 0;
//     let part = 1;
//     while(num <= nsquare) {
//         if(part == 1) {
//             x = 0;
//             y = 0;
//         } else if(part % 2 == 1) {
//             y = top;
//             x = 0;
//             top ++;
//         } else {
//             y = 0;
//             x = bottom;
//             bottom --;
//         }
//         while(x < n && y < n) {
//             mat[x][y] = num;
//             x++;
//             y++;
//             num++;
//         }
//         part++;
//     }
//     for(let k = 0; k < n; k++) {
//         let res = mat[k].join(' ');
//         print(res);
//     }
// }

var n = 2;
const mat = new Array();
for (let i = 0; i < n; i++) {
    mat[i] = new Array(n);
}
let bottom = n - 1;
let top = 1;
let num = 1;
let nsquare = n * n;
let x = 0,
    y = 0;
let part = 1;
while (num <= nsquare) {
    if (part == 1) {
        x = 0;
        y = 0;
    } else if (part % 2 == 0) {
        y = top;
        x = 0;
        top++;
    } else {
        y = 0;
        x = bottom;
        bottom--;
    }
    while (x < n && y < n) {
        mat[x][y] = num;
        x++;
        y++;
        console.log(num);
        num++;
    }
    part++;
}
for (let k = 0; k < n; k++) {
    let res = mat[k].join(' ');
    console.log(res);
}