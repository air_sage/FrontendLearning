/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var maxHeapify = function(a, i, heapSize) {
    let l = i * 2 + 1,
        r = i * 2 + 2,
        largest = i;
    if (l < heapSize && a[l] > a[largest]) {
        largest = l;
    }
    if (r < heapSize && a[r] > a[largest]) {
        largest = r;
    }
    if (largest !== i) {
        [a[largest], a[i]] = [a[i], a[largest]];
        maxHeapify(a, largest, heapSize);
    }
}

var buildMaxHeap = function(a, heapSize) {
    for (let i = heapSize / 2; i >= 0; --i) {
        maxHeapify(a, i, heapSize);
    }
}

// var arrUnique = function(arr) {
//     const set = new Set();
//     const res = [];
//     for(let i = 0; i < arr.length; i++) {
//         if(!set.has(arr[i])) {
//             res.push(arr[i]);
//             set.add(arr[i]);
//         }
//     }
//     return res;
// }

var findKthLargest = function(nums, k) {
    // const arr = arrUnique(nums);
    let heapSize = nums.length;
    const temp = new Set();
    buildMaxHeap(nums, heapSize);
    for (let i = nums.length - 1; i >= nums.length - k + 1; i--) {
        [nums[0], nums[i]] = [nums[i], nums[0]];
        if (temp.has(nums[0] + '')) {} else {
            temp.add(nums[0] + '');
        }
        heapSize--;
        maxHeapify(nums, 0, heapSize);
    }
    return nums[0];
}
const arr = [-1, 2, 0];
let k = 1;
let res = findKthLargest(arr, k);
console.log(res);