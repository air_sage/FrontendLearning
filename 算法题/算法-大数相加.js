function add(a, b) {
    let len1 = a.length;
    let len2 = b.length;
    a = a.split('').reverse();
    b = b.split('').reverse();
    let res = [];
    let str;
    let str1;
    let flag = 0;
    if (len1 > len2) {
        str = a;
        str1 = b;
    } else {
        str = b;
        str1 = a;
    }
    for (let i = 0; i < Math.max(len1, len2); i++) {
        let temp1 = parseInt(str[i]);
        let temp2 = i >= len2 ? 0 : parseInt(str1[i]);
        let sum = temp1 + temp2 + flag;
        if (sum >= 10) {
            flag = 1;
            sum -= 10;
        } else {
            flag = 0;
        }
        res.push(sum);
    }
    console.log(res);
    if (flag === 1) {
        res.push(flag);
    }
    return res.reverse().join('');
}

console.log(add('199', '88'));