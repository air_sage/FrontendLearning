const data = [{
        "id": "1897e132-e98e-4d33-9ca4-b884d4079567",
        "images": [
            "86fe49a4-9124-4ded-8e28-e085f39df0c7",
            "683b7f9b-4b04-4b72-a590-95413821e7a3"
        ],
        "name": "分组-1",
        "parent_id": null
    },
    {
        "id": "fb88c694-df36-42db-9890-429b58674877",
        "images": [
            "d334e881-9158-4c4b-a6f4-99aaae436b48",
            "d54c4f2c-7022-4279-836e-fb7920bc8117",
            "8fc70dcd-ee43-4ce8-8444-1ad6e69097aa"
        ],
        "name": "分组-2",
        "parent_id": null
    },
    {
        "id": "685a99b5-f8dd-427f-a6df-e43766358e68",
        "images": [
            "ff53d659-0a42-40b7-ae0d-e72e908225fb"
        ],
        "name": "分组-1-1",
        "parent_id": "1897e132-e98e-4d33-9ca4-b884d4079567"
    },
    {
        "id": "7c93d559-8c20-4480-83aa-c9fc51c066f0",
        "images": [],
        "name": "分组-3",
        "parent_id": null
    },
    {
        "id": "703faaa2-c3ae-49a4-9ebd-c152adc6f91c",
        "images": [
            "c6a5f008-4c8d-4b4f-96bc-a34dee45f2ab",
            "140a6589-396c-4d5b-bcc5-69298ac50926",
            "1801109b-89d4-4846-b330-bcf71e55aa33"
        ],
        "name": "分组-1-2",
        "parent_id": "1897e132-e98e-4d33-9ca4-b884d4079567"
    },
    {
        "id": "b22a266d-9e6a-4ab4-ab6d-4d3fad1e41f0",
        "images": [
            "e35b64ea-356e-4324-be4d-4827b8850396"
        ],
        "name": "分组-1-3",
        "parent_id": "1897e132-e98e-4d33-9ca4-b884d4079567"
    },
    {
        "id": "08b7bf26-51f9-43f6-bb46-892397d5f210",
        "images": [
            "fff754fe-71d5-4ccb-9bc6-9417c72cf1db",
            "c6a5f008-4c8d-4b4f-96bc-a34dee45f2ab"
        ],
        "name": "分组-1-1-1",
        "parent_id": "685a99b5-f8dd-427f-a6df-e43766358e68"
    }
]

var arrToTree = function(arr) {
    const mp = new Map();
    const res = [];
    for (let i = 0; i < arr.length; i++) {
        mp.set(arr[i].id, arr[i]);
    }
    for (let i = 0; i < arr.length; i++) {
        if (mp.get(arr[i].parent_id)) {
            if (!mp.get(arr[i].parent_id).child) {
                mp.get(arr[i].parent_id).child = [];
            }
            mp.get(arr[i].parent_id).child.push(arr[i]);
        }
    }
    for (let item of mp.values()) {
        if (item.parent_id == null) {
            res.push(item);
        }
    }
    return res;
}

let result = arrToTree(data);
console.log(result);