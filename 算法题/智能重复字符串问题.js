// smartRepeat智能重复字符串问题
// 将 '3[abc]' 变为 'abcabcabc'
// 将 '3[2[a]2[b]]' 变成 'aabbaabbaabb'
// 将 '2[1[a]3[b]2[3[c]4[d]]]' 变成 'abbbcccddddcccddddabbbcccddddcccdddd'

function smartRepeat(str, res) {
    const time = [];
    const content = [[]]; // 先预先加一个空数组防止下面找栈顶出错的问题
    let number = /\d/;
    let string = /[a-zA-Z]/;
    for (let i = 0; i < str.length; i++) {
        if (number.test(str[i])) {
            time.push(parseInt(str[i]));
            content.push([]);
        } else if (string.test(str[i])) {
            content[content.length - 1].push(str[i]);
        } else if (str[i] === ']') {
            let tempTime = time.pop();
            let tempContent = content.pop();
            for (let j = 0; j < tempTime; j++) {
                content[content.length - 1].push(tempContent.join(''));
            }
        }
    }
    console.log(content[0].join(''));
    console.log(res);
    console.log(content[0].join('') === res);
}

let arr = [
    ['3[abc]', 'abcabcabc'],
    ['3[2[a]2[b]]', 'aabbaabbaabb'],
    ['2[1[a]3[b]2[3[c]4[d]]]', 'abbbcccddddcccddddabbbcccddddcccdddd']
];

arr.forEach(item => {
    smartRepeat(item[0], item[1]);
})
