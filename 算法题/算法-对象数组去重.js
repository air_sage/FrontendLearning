let arr = [{ a: 1 }, { a: 2, b: 2 }, { a: 2 }];

function unique(arr) {
    let temp = [];
    arr.forEach(item => {
        temp.push(item.a);
    })
    for (let j = 0; j < temp.length; j++) {
        if (temp[j] !== null) {
            for (let k = j + 1; k < temp.length; k++) {
                if (temp[k] == temp[j]) {
                    temp[k] = null;
                }
            }
        }
    }
    let res = [];
    for (let i = 0; i < temp.length; i++) {
        if (temp[i] !== null) {
            res.push(arr[i]);
        }
    }
    return res;
}

console.log(unique(arr));