var addStrings = function(num1, num2) {
    // 声明两个字符串的长度
    let i = num1.length - 1;
    let j = num2.length - 1;

    // 声明长的字符串
    let lenMax;
    let lenMin;

    let numMax;
    let numMin;
    let res = ''
    if (i < j) {
        lenMax = j;
        lenMin = i;
        numMax = num2;
        numMin = num1;
    } else {
        lenMax = i;
        lenMin = j;
        numMax = num1;
        numMin = num2;
    }
    let diff = lenMax - lenMin;
    let flag = 0;

    for (let k = lenMax; k >= 0; k--) {
        let temp;
        if (lenMin >= 0) {
            temp = parseInt(numMax[k]) + parseInt(numMin[k - diff]) + flag;
        } else {
            temp = parseInt(numMax[k]) + flag;
        }
        lenMin--;
        flag = 0;
        if (temp > 9) {
            temp = temp - 10;
            flag = 1;
        }
        res = temp + res;
    }
    return flag ? flag + res : res;
};

let res = addStrings("11", "123");
console.log(res);

var addStrings1 = function(num1, num2) {
    let res = "";
    let i = num1.length - 1,
        j = num2.length - 1;
    let flag = 0;
    while (i >= 0 || j >= 0) {
        let n1 = i >= 0 ? parseInt(num1[i]) : 0;
        let n2 = j >= 0 ? parseInt(num2[j]) : 0;
        let temp = n1 + n2 + flag;
        flag = Math.floor(temp / 10);
        res = String(temp % 10) + res;
        [i, j] = [i - 1, j - 1];
    }
    return flag ? flag + res : res;
}