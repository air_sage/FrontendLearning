function getFraction(a) {
    let min = 1;
    let res = {};
    for (let i = 1; i <= 200; i++) {
        for (let j = 1; j <= 200; j++) {
            let temp = i / j;
            let diff = Math.abs(temp - a);
            if (diff < min || diff == min && i + j < res.top + res.bottom) {
                min = diff
                res.top = i;
                res.bottom = j;
            }
        }
    }
    return [res.top, res.bottom];
}

let result = getFraction(0.9545);
console.log(result);