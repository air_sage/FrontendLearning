function Node(val, left, right) {
    this.val = val;
    this.left = left;
    this.right = right;
}

function buildTree(root, n) {
    if (root.val === n) return;
    let val = root.val;
    let left = new Node(val + 1, null, null);
    let right = new Node(val + 1, null, null);
    root.left = left;
    root.right = right;
    buildTree(left, n);
    buildTree(right, n);
    return root;
}

function levelPrint(root) {
    let q = [];
    q.push(root);
    let res = [];
    while (q.length) {
        let node = [];
        while (q.length) {
            node.push(q.shift());
        }
        let res1 = [];
        node.forEach(item => {
            res1.push(item.val);
            if (item.left) q.push(item.left);
            if (item.right) q.push(item.right);
        })
        res.push(res1);
    }
    return res;
}

let root = new Node(0, null, null);
let res = buildTree(root, 4);
console.log(res);
console.log(levelPrint(res));