// 链表构造函数
function ListNode(val, next) {
    this.val = (val === undefined ? 0 : val);
    this.next = (next === undefined ? null : next);
}

// 创建链表
function createList(arr) {
    let node = new ListNode(0);
    let p = node;
    for (let i = 0; i < arr.length; i++) {
        let n = new ListNode(arr[i], null)
        p.next = n;
        p = p.next;
    }
    return node.next;
}

// 链表转数组
function listToArray(node) {
    let arr = [];
    let p = node;
    while (p) {
        arr.push(p.val);
        p = p.next;
    }
    return arr;
}

// 法一
var mergeTwoLists = function(l1, l2) {
    var node = new ListNode(0);
    var p = node;
    if (!l1 || !l2) return !l1 ? l2 : l1;
    while (l1 && l2) {
        if (l1.val <= l2.val) {
            p.next = l1;
            l1 = l1.next;
            p = p.next;
        } else {
            p.next = l2;
            l2 = l2.next;
            p = p.next;
        }
    }
    if (l1) {
        p.next = l1;
    }
    if (l2) {
        p.next = l2;
    }
    return node.next;
};

// 法二
var mergeTwoLists2 = function(l1, l2) {
    if (!l1 && !l2) return l1;
    if (!l1 || !l2) return !l1 ? l2 : l1;

    if (l1.val < l2.val) {
        l1.next = mergeTwoLists2(l1.next, l2);
        return l1;
    } else {
        l2.next = mergeTwoLists2(l1, l2.next);
        return l2;
    }
}

let node1 = createList([1, 2, 4]);
let node2 = createList([1, 3, 4]);
console.log(node1);
console.log(node2);
let resNode = mergeTwoLists2(node1, node2);

let res = listToArray(resNode);
console.log(res);