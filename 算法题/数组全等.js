const a1 = [1, 2, 3, 4, { a: 1 }];
// const a2 = [...a1];
// 浅拷贝 + 副本(pop()和push()之类不影响)
const a2 = Array.from(a1);
const a3 = a1.slice();
a2[4].a = 2;

console.log(a1[4] === a2[4]);
console.log(a1[4]);
a1.pop();
a1.push(1);
console.log(a1);
console.log(a2, a3);
a2[4].a = 3;
console.log(a2, a3);
a2.pop();

console.log(a2, a3);