// 判断情况一和情况二分别输出什么
Promise.resolve(1)
    .then(x => x + 1)
    .then(x => {
        // 情况一 抛出错误
        throw new Error('my error')
        // 情况二 正常返回
        // return x;
    })
    .catch(() => 1)
    .then(x => x + 1)
    .then(x => console.log(x))
    .catch(console.error)