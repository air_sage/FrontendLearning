console.log(1);
setTimeout(() => {
    console.log(2);
}, 20);
console.log(3);
setTimeout(() => {
    console.log(4);
}, 0);
console.log(5);
setTimeout(() => {
    console.log(6);
}, 10);
console.log(9);
setTimeout(() => {
    console.log(10);
}, 10);

// for (var i = 0; i < 99999999; i++) {}
console.log(7);
setTimeout(() => {
    console.log(8);
}, 10);
console.log(11);
setTimeout(() => {
    console.log(12);
}, 0);