async function async1() {
    console.log("async1 start");
    await async2();
    console.log("async1 end");
}
async function async2() {
    console.log('async2');
    return new Promise(function(resolve, reject) {
        resolve();
        console.log('async2 promise');
    }).then(() => { console.log('async2 then'); })
}
console.log("script start");
setTimeout(function() {
    console.log("settimeout");
}, 0);
async1();
new Promise(function(resolve) {
    console.log("promise1");
    resolve();
}).then(function() {
    console.log("promise2");
}).then(function() {
    console.log("promise3");
}).then(function() {
    console.log("promise4");
})
console.log('script end');