async function async1() {
    console.log('async1 start')
    await async2()
        // await new Promise(resolve => {
        //         console.log('promise1')
        //         resolve()
        //     })
        //     .then(() => {
        //         console.log('promise2')
        //     });
    console.log('async1 end')
}
// 注意是否在函数中，下面是否有连续调用then
// async2()里面加上return相当于直接在await后面 new Promise，即 await async2() <==> await new Promise
// 不加return ，await async2()结果会不一样, 即 await async2() !== await new Promise
// 加await 和 return
async function async2() {
    return new Promise(resolve => {
            console.log('promise1')
            resolve()
        })
        .then(() => {
            console.log('promise2')
        })
}

// // 注意 有无return的区别 
// async function async2() {
//     new Promise(resolve => {
//             console.log('promise1')
//             resolve()
//         })
//         .then(() => {
//             console.log('promise2')
//         })
// }


console.log('script start')
setTimeout(() => {
    console.log('setTimeout')
}, 0);
async1()
new Promise(resolve => {
    console.log('promise3')
    resolve()
}).then(() => {
    console.log('promise4')
}).then(() => {
    console.log('promise6');
}).then(() => {
    console.log('promise7');
})
new Promise(resolve => {
    console.log('promise8')
    resolve()
}).then(() => {
    console.log('promise9')
}).then(() => {
    console.log('promise10');
}).then(() => {
    console.log('promise11');
})
console.log('script end')