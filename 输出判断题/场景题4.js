var a = new Object(),
    c = [1];
a.value = 1;
var b = a,
    d = c;
// a.value = 2;
a = { value: 3 };
c = [2];
console.log(b);
console.log(d);

// function A(name) {
//     this.name = name;
// }
// A.prototype = {
//     sayHello: function() {
//         console.log(this.name + ',hello');
//     }
// }

// console.log(A('airsage').sayHello());