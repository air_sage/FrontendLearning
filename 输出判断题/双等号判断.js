console.log('[] == 0', [] == 0); // true
// 1.因为等号左边是引用数据类型要转换
// 2.优先使用valueOf(),由于valueOf() 返回的是[]，依旧是一个对象，接下来就使用toString
// 3.[].toString()返回的是空字符串"",所以比较变成了 "" == 0;
// 4.字符串再进行转换，空字符串转为数字0， 0 === 0 为true。

console.log('null == 0', null == 0); // false
console.log('null == false', null == false); // false
console.log('false == undefined', false == undefined); // false
console.log('null == undefined', null == undefined); // true
console.log('[] == "0"', [] == '0'); // false
console.log('[] == ""', [] == ''); // true
console.log('typeof [].valueOf()', typeof [].valueOf()); // string
console.log('typeof [].toString()', typeof [].toString()); // object
console.log('[] == []', [] == []); // false
console.log('{} == {}', {} == {}); // false
console.log('[] == {}', [] == {}); // false
console.log('![] == []', ![] == []); //true
console.log('!![]', !![]); // true
console.log('!{} == {}', !{} == {}); //false
console.log('!{} == []', !{} == []); // true
console.log('Boolean([])', Boolean([])); // true
console.log('Boolean({})', Boolean({})); // true

console.log('{} == false', {} == false); // false

console.log('{} == 0', {} == 0);
// 1.左边为对象，使用valueOf()，返回的是一个空字符串
console.log('{}.valueOf()', {}.valueOf()); // {}
console.log('{}.toString()', {}.toString()); // "[object Object]"
// 2.所以要调用toString()，返回的是字符串"[object Object]",
console.log('Number("[object Object]")', Number("[object Object]")); // NaN
// 3."[object Object]" == 0,字符串要转换为数字NaN， NaN === 0,结果为false

console.log('Boolean(NaN)', Boolean(NaN));
console.log('!NaN == false', !NaN == false);