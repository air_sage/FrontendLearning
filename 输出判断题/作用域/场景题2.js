function Foo() {
    log = function() {
        console.log(1);
    };
    return this;
}
Foo.log = function() {
    console.log(2);
}
Foo.prototype.log = function() {
    console.log(3);
}
var log = function() {
    console.log(4);
}

function log() {
    console.log(5);
}

Foo.log(); // 2
log(); // 4
Foo().log(); // 1
log(); // 1
new Foo.log(); // 2
new Foo().log(); // 3