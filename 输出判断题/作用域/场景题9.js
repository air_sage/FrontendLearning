// function f() {
//     var a = 1;

//     a = 2;
//     var b = g();
//     // console.log(b);
//     a = 3;

//     return b;

//     function g() {
//         return a;
//     }
// }

var f1 = f();
console.log(f1);

function foo() {
    // okay to capture 'a'
    return a;
}

// 不能在'a'被声明前调用'foo'
// 运行时应该抛出错误
foo();

let a;